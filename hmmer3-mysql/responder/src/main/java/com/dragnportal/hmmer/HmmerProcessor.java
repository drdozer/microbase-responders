/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:45:59
 */

package com.dragnportal.hmmer;

import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;

/**
 *
 * @author Keith Flanagan
 */
public class HmmerProcessor
    extends AbstractMessageProcessor
{
  private static final Logger l = Logger.
      getLogger(HmmerProcessor.class.getSimpleName());
  private static final String TOPIC_IN_PREFIX = "-RUN_COMMAND";
  private static final String TOPIC_OUT_PREFIX = "-COMPLETED";
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(300);
    info.setMaxLocalCores(50);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(HmmerProcessor.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(info.getResponderId()+TOPIC_IN_PREFIX);
    return info;
  }
  
  protected String getOutgoingTopic()
  {
    String outTopic = getResponderInfo().getResponderId()+TOPIC_OUT_PREFIX;
    return outTopic;
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    l.info("Starting stage: preRun for message: "+message.getGuid()+", "+message.getTopicGuid()+", part of work: "+message.getWorkflowExeId()+":"+message.getWorkflowStepId());
    
    /*
     * Here, we read the incoming message and perform a number of actions:
     * 1) Decide whether we've seen this message before (i.e., the message is
     *    being reprocessed. In this case, existing (potentially partial) 
     *    datasets relating to this message must be deleted.
     * 2) Based on the input data, decide what the output file(s) will be
     *    called, and where they will be uploaded to.
     */
    
    
//    pair = parseMessage(message);
//    
//    /*
//     * Determine what the remote S3 location of the output files generated
//     * by this responder will be.
//     */
//    String remoteS3Bucket = pair[0].getBucket();
//    String remoteS3Path = "pairwise-blasts/"
//                              +pair[0].getName()+"-vs-"+pair[1].getName();
//    String remoteS3Name = pair[0].getName()+"-"+pair[1].getName()+".blast";
//    blastReportS3File = new FileMetaData(
//        remoteS3Bucket, remoteS3Path, remoteS3Name);
//    
//    logFileS3Bucket = remoteS3Bucket;
//    logFileS3Path = remoteS3Path;
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Set<Message> processMessage(Message message)
      throws ProcessingException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

}
