/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.full.data;

import java.io.Serializable;

/**
 *
 * @author Sirintra Nakjang
 */
public class GenomeFragmentSynonym
        implements Serializable
{
  //private GenomeFragment genomeFragment;

  private String synonym;

  public GenomeFragmentSynonym()
  {
  }

//  public GenomeFragment getGenomeFragment()
//  {
//    return genomeFragment;
//  }
//
//  public void setGenomeFragment(GenomeFragment genomeFragment)
//  {
//    this.genomeFragment = genomeFragment;
//  }

  public String getSynonym()
  {
    return synonym;
  }

  public void setSynonym(String synonym)
  {
    this.synonym = synonym;
  }
}
