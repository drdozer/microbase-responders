/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.mucosa.genomepool.full.data.FragmentType;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;

/**
 *
 * @author Keith Flanagan
 */
public class ResultSetMappingDAO
{

  public ResultSetMappingDAO()
  {
  }

  
  public boolean messageIdExists(Connection txn, String messageGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM resultset_mapping WHERE msg_guid = ?");
      stmt.setString(1, messageGuid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute messageIdExists() for message: "+messageGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }


  public boolean dataExists(Connection txn, 
      String fileBucket, String filePath, String fileName)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM resultset_mapping WHERE "
          + "file_bucket = ? AND file_path = ? AND file_name = ?");
      int col = 1;
      stmt.setString(col++, fileBucket);
      stmt.setString(col++, filePath);
      stmt.setString(col++, fileName);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute messageIdExists() for data: "
          + fileBucket +"; " + filePath + "; " + fileName, e);
    }
    finally
    {
      stmt.close();
    }
  }

  public int count(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM resultset_mapping");
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute count()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public int count(Connection txn, int taxonId)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM genome_fragments where taxon_id = ?");
      stmt.setInt(1, taxonId);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute count() for taxon_id: "+taxonId, e);
    }
    finally
    {
      stmt.close();
    }
  }

  public List<String> getGuids(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT guid FROM genome_fragments");
      List<String> result = new ArrayList<String>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        result.add(rs.getString(1));
      }
      return result;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getGuids()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public GenomeFragment getByGuid(Connection txn, String guid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM genome_fragments WHERE guid = ?");
      stmt.setString(1, guid);
      ResultSet rs = stmt.executeQuery();
      List<GenomeFragment> fragments = resultSetToGenomeFragments(rs);
      if (fragments.size() == 1)
      {
        return fragments.get(0);
      }
      else if (fragments.size() == 0)
      {
        return null;
      }
      else
      {
        throw new SQLException(fragments.size()+" GenomeFragment instances had "
            + "the same GUID: "+guid + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByGuid()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public List<GenomeFragment> list(Connection txn, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      String sql = "SELECT * FROM genome_fragments "
          + "ORDER BY organism_name LIMIT ? OFFSET ?";
      stmt = txn.prepareStatement(sql.toString());
      stmt.setInt(1, limit);
      stmt.setInt(2, offset);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGenomeFragments(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute list()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public List<GenomeFragment> listByTaxonId(Connection txn, int taxonId)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM genome_fragments WHERE taxon_id = ?");
      stmt.setInt(1, taxonId);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGenomeFragments(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByTaxonId()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public List<GenomeFragment> listByTaxonIds(
      Connection txn, List<Integer> taxonIds, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      if (taxonIds.isEmpty())
      {
        throw new SQLException("List taxonIds was empty!");
      }
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT * FROM genome_fragments WHERE taxon_id IN (");
      for (int taxonId : taxonIds)
      {
        sql.append(taxonId);
        sql.append(",");
      }
      sql.deleteCharAt(sql.length()-1);
      sql.append(" ) ORDER BY organism_name LIMIT ? OFFSET ?");
      stmt = txn.prepareStatement(sql.toString());
      stmt.setInt(1, limit);
      stmt.setInt(2, offset);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGenomeFragments(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute listByTaxonIds()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public int countByTaxonIds(Connection txn, List<Integer> taxonIds)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      if (taxonIds.isEmpty())
      {
        throw new SQLException("List taxonIds was empty!");
      }
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT COUNT(*) as count FROM genome_fragments WHERE taxon_id IN (");
      for (int taxonId : taxonIds)
      {
        sql.append(taxonId);
        sql.append(",");
      }
      sql.deleteCharAt(sql.length()-1);
      sql.append(" )");
      stmt = txn.prepareStatement(sql.toString());
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute countByTaxonIds()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public List<GenomeFragment> listFragmentsContainingPartialOrgName(
      Connection txn, String partialOrganismName, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      StringBuilder likeString = new StringBuilder("%");
      likeString.append(partialOrganismName).append("%");
      stmt = txn.prepareStatement(
        "SELECT * FROM genome_fragments WHERE organism_name LIKE ? "
        + "ORDER BY organism_name LIMIT ? OFFSET ?");
      stmt.setString(1, likeString.toString());
      stmt.setInt(2, offset);
      stmt.setInt(3, limit);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGenomeFragments(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute listFragmentsContainingPartialOrgName()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public List<GenomeFragment> listFragmentsOrderByOrganismName(
      Connection txn, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
          "SELECT * FROM genome_fragments ORDER BY organism_name LIMIT ? OFFSET ?");
      stmt.setInt(1, offset);
      stmt.setInt(2, limit);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGenomeFragments(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute listFragmentsOrderByOrganismName()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  private static List<GenomeFragment> resultSetToGenomeFragments(ResultSet rs)
      throws SQLException
  {
    List<GenomeFragment> list = new ArrayList<GenomeFragment>();
    while(rs.next())
    {
      GenomeFragment gf = new GenomeFragment();
      gf.setAccession(rs.getString("accession"));
      gf.setComment(rs.getString("comment"));
      gf.setDescription(rs.getString("description"));
      gf.setDnaSequenceLength(rs.getInt("dna_sequence_length"));
      gf.setFragmentSubtype(rs.getString("fragment_subtype"));
      gf.setFragmentType(FragmentType.valueOf(rs.getString("fragment_type")));
      gf.setGiNumber(rs.getInt("gi"));
      gf.setGuid(rs.getString("guid"));
      gf.setNcbiModified(null);
      gf.setNumberProtein(rs.getInt("number_protein"));
      gf.setOrganismName(rs.getString("organism_name"));
      gf.setOrganismShortName(rs.getString("organism_short_name"));
//      gf.setParsedFrom(null);
      gf.setTaxonId(rs.getInt("taxon_id"));
      gf.setVersion(rs.getString("version"));

      list.add(gf);
    }
    return list;
  }
}
