/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.notification;

/**
 *
 * @author Keith Flanagan
 */
public class NotificationConstants
{
  //Incoming topics
  public static final String IN_TOPIC_GBK_FOUND = "GenbankParserProcessor.new_GBK_file";
  public static final String IN_TOPIC_GENOMIC_GBFF_FOUND = "GenbankParserProcessor.new_GENOMIC_GBFF_file";
  public static final String IN_TOPIC_FAA_FOUND = "filescanner.out.new_FAA_file";
  public static final String IN_TOPIC_FASTQ_FOUND = "filescanner.out.new_FASTQ_file";

  //Internal topics
//  public static final String INT_TOPIC_FAA_JOB = "genomepool.internal.parse_FAA_job";
//  public static final String INT_TOPIC_GBK_JOB = "genomepool.internal.parse_GBK_job";

  //Outgoing topics
  public static final String OUT_TOPIC_FAA_PARSED = "genomepool.out.parsed_FAA";
  public static final String OUT_TOPIC_FASTQ_PARSED = "genomepool.out.parsed_FASTQ";
  public static final String OUT_TOPIC_GBK_PARSED = "genomepool.out.GBK_available";
  public static final String OUT_TOPIC_GBK_PARSE_FAILURE = "genomepool.out.GBK_parse_failure";
  public static final String OUT_TOPIC_GENOMIC_GBFF_PARSED = "genomepool.out.parsed_GENOMIC_GBFF";
}
