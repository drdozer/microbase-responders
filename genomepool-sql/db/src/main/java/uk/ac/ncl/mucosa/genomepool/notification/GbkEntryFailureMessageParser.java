/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package uk.ac.ncl.mucosa.genomepool.notification;

import java.io.Serializable;
import uk.org.microbase.notification.data.Message;

/**
 * 
 * @author Keith Flanagan
 */
public class GbkEntryFailureMessageParser
    implements Serializable
{
  private static final String PARSED_FROM_BUCKET = "parsed_from_bucket";
  private static final String PARSED_FROM_PATH = "parsed_from_path";
  private static final String PARSED_FROM_NAME = "parsed_from_name";
  private static final String PARSED_FROM_INDEX = "parsed_from_index";
  
  private static final String EXCEPTION = "exception";


  protected final Message message;
  

  public GbkEntryFailureMessageParser(Message message)
  {
    this.message = message;
  }

  public Message getMessage()
  {
    return message;
  }

  

  public String getException()
  {
    return message.getContent().get(EXCEPTION);
  }

  public void setException(String exception)
  {
    message.getContent().put(EXCEPTION, exception);
  }


  public String getParsedFromBucket()
  {
    return message.getContent().get(PARSED_FROM_BUCKET);
  }

  public void setParsedFromBucket(String parsedFromBucket)
  {
    message.getContent().put(PARSED_FROM_BUCKET, parsedFromBucket);
  }

  public String getParsedFromName()
  {
    return message.getContent().get(PARSED_FROM_NAME);
  }

  public void setParsedFromName(String parsedFromName)
  {
    message.getContent().put(PARSED_FROM_NAME, parsedFromName);
  }

  public String getParsedFromPath()
  {
    return message.getContent().get(PARSED_FROM_PATH);
  }

  public void setParsedFromPath(String parsedFromPath)
  {
    message.getContent().put(PARSED_FROM_PATH, parsedFromPath);
  }

  public String getParsedFromIndex()
  {
    return message.getContent().get(PARSED_FROM_INDEX);
  }

  public void setParsedFromIndex(String parsedFromIndex)
  {
    message.getContent().put(PARSED_FROM_INDEX, parsedFromIndex);
  }

}
