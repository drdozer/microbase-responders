/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;

import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows removal of all data relating to a specified genome fragment. This
 * is useful when a job fails to process correctly and needs to be rolled back.
 * 
 * This implementation assumes correct 'ON CASCADE DELETE' behaviour from the
 * underlying database.
 * 
 * @author Keith Flanagan
 */
public class GenomeFragmentRemover
{
  private static final Logger logger = 
      Logger.getLogger(GenomeFragmentRemover.class.getName());


  
  public static void removeGenomeFragment(Connection txn, String fragmentGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      logger.log(Level.INFO, "Going to delete all information "
          + "relating to fragment: {0}", fragmentGuid);
      final String sql =
          "DELETE FROM genome_fragments WHERE guid = ?";

      stmt = txn.prepareStatement(sql);
      int col = 1;
      stmt.setString(col++, fragmentGuid);
      int rows = stmt.executeUpdate();
      logger.log(Level.INFO, "All information for fragment: {0} was deleted. "
          + "Total rows affected: {1}", new Object[]{fragmentGuid, rows});
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException(
          "Failed to delete GenomeFragment entry: "+fragmentGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  private static void removeResultSetMappingsForMessage(
      Connection txn, String messageGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "DELETE FROM resultset_mapping "
          + "WHERE msg_guid = ?";

      stmt = txn.prepareStatement(sql);
      int col = 1;
      stmt.setString(col++, messageGuid);
      
      stmt.executeUpdate();
      
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException(
          "Failed to delete result set mappings for notification: "+messageGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public static void removeByMessageGuid(Connection txn, String messageGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      logger.log(Level.INFO, "Going to delete all information "
          + "populated as a result of message: {0}", messageGuid);
      final String sql =
          "SELECT id FROM resultset_mapping "
          + "WHERE msg_guid = ?";
//          "SELECT fragment_guid FROM notification_data_mapping "
//          + "WHERE message_guid = ?";

      stmt = txn.prepareStatement(sql);
      int col = 1;
      stmt.setString(col++, messageGuid);
      
      GenomeFragmentDAO fragDao = new GenomeFragmentDAO();
      ResultSet rs = stmt.executeQuery();
      while(rs.next()) {
        // Find the fragment(s) that were added as a result of this message
        int workflowId = rs.getInt("id");
        List<String> fragGuids = fragDao.getGuidsForWorkflowId(txn, workflowId);
        
        // Delete those fragments, and all dependent entries in other tables
        for (String fragGuid : fragGuids) {
          removeGenomeFragment(txn, fragGuid);    
        }
      }
      
      //Finally, remove the result set mapping entries
      removeResultSetMappingsForMessage(txn, messageGuid);
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException(
          "Failed to delete data relating to notification: "+messageGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }
}
