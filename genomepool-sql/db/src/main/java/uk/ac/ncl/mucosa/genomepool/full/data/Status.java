/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation. The full license may be found in
 *  COPYING.LESSER in this project's root directory.
 * 
 *  Copyright 2009 jointly held by the authors listed at the top of each
 *  source file and/or their respective employers.
 */

package uk.ac.ncl.mucosa.genomepool.full.data;

import java.io.Serializable;

/**
 *
 * @author Keith Flanagan
 */
public enum Status
        implements Serializable
{
  CURRENTLY_USED,
  CURRENTLY_NOT_USED;
}
