--
-- GenomePool SQL schema
--
-- Author: Sirintra Nakjang, Keith Flanagan
--

-- A table that enables failed jobs to be re-executed without causing database
-- inconsistencies. Also allows duplicate message content (same request in a 
-- different message) to be detected and (in this case) ignored.
-- This table maps incoming message IDs and content to and 'id' field.
-- The 'id' field is then stored or associated with every database row that is
-- added as a resut of executing the job. 
CREATE TABLE resultset_mapping (
    id INT NOT NULL AUTO_INCREMENT UNIQUE,
    msg_guid character varying(254),
    workflow_exe_id character varying(254),
    workflow_step_id character varying(254),

    file_bucket character varying(254),
    file_path character varying(254),
    file_name character varying(254),

    created timestamp DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
);


CREATE TABLE genome_fragments (
	id INT NOT NULL AUTO_INCREMENT UNIQUE,
    workflow_id INT,
	guid character varying(255) NOT NULL UNIQUE,
	gi integer,
	taxon_id integer NOT NULL,
	organism_name character varying(255),
	organism_short_name character varying(255),
	accession character varying(20),
	version character varying(20),
	description character varying(255) DEFAULT '',
	comment mediumtext,
        number_protein integer,
        fragment_type character varying(50),
        fragment_subtype character varying(255),
        created timestamp DEFAULT CURRENT_TIMESTAMP ,
        dna_sequence_length integer,

  PRIMARY KEY (guid),
  FOREIGN KEY (workflow_id) REFERENCES resultset_mapping(id) ON DELETE CASCADE
);
CREATE INDEX genome_fragments__gi_idx using btree ON genome_fragments(gi);
CREATE INDEX genome_fragments__taxon_id_idx using btree ON genome_fragments(taxon_id) ;
CREATE INDEX genome_fragments__organism_name_idx using btree ON genome_fragments(organism_name);
CREATE INDEX genome_fragments__accession_idx using btree ON genome_fragments(accession);
CREATE INDEX genome_fragments__version_idx using btree ON genome_fragments(version);


CREATE TABLE dna_sequences (
    taxon_id integer NOT NULL,
    fragment_guid character varying(255) NOT NULL,
    dna_sequence longtext NOT NULL,

    PRIMARY KEY (fragment_guid),
    FOREIGN KEY (fragment_guid) REFERENCES genome_fragments(guid) ON DELETE CASCADE
);
CREATE INDEX dna_sequences__taxon_id_idx using btree ON dna_sequences(taxon_id);

CREATE TABLE genes (
    id INT NOT NULL AUTO_INCREMENT UNIQUE,
    guid character varying(255) NOT NULL,
    gene_id character varying(255),
    gene_name character varying(255),
    taxon_id integer NOT NULL,	
    fragment_guid character varying(255) NOT NULL,
    locus_tag character varying(255),
    start_locus integer DEFAULT 0 NOT NULL,
    end_locus integer DEFAULT 0 NOT NULL,
    orientation character varying(1) DEFAULT '+' NOT NULL,
    CONSTRAINT genes_end_locus_check CHECK (end_locus >= 0),
    CONSTRAINT genes_start_locus_check CHECK (start_locus >= 0),
    CONSTRAINT genes_orientation_check CHECK ((orientation) in ('+','-')),
    PRIMARY KEY (guid),
    FOREIGN KEY (fragment_guid) REFERENCES genome_fragments(guid) ON DELETE CASCADE
);
CREATE INDEX genes__gene_id_idx using btree ON genes(gene_id);
CREATE INDEX genes__gene_name_idx using btree ON genes(gene_name);
CREATE INDEX genes__taxon_id_idx using btree ON genes(taxon_id);
CREATE INDEX genes__fragment_guid_idx using btree ON genes(fragment_guid);
CREATE INDEX genes__locus_tag_idx using btree ON genes(locus_tag);
CREATE INDEX genes__start_locus_idx using btree ON genes(start_locus);
CREATE INDEX genes__end_locus_idx using btree ON genes(end_locus);



CREATE TABLE gene_products (
    id INT NOT NULL AUTO_INCREMENT UNIQUE,
    guid character varying(255) NOT NULL,
    gene_guid character varying(255) NOT NULL,
    gi integer DEFAULT 0 NOT NULL,
    protein_id character varying(255),
    tagid character varying(100),
    taxon_id integer NOT NULL,
    fragment_guid character varying(255) NOT NULL,
    product_type character varying(7) DEFAULT 'protein' NOT NULL,
    description mediumtext,
    product_sequence mediumtext,
    CONSTRAINT gene_products_product_type_check CHECK ((product_type) in ('RNA', 'protein')),
    
    PRIMARY KEY (guid),
    FOREIGN KEY (gene_guid) REFERENCES genes(guid) ON DELETE CASCADE 
);
CREATE INDEX gene_products__gene_guid_idx using btree ON gene_products(gene_guid);
CREATE INDEX gene_products__gi_idx using btree ON gene_products(gi);
CREATE INDEX gene_products__protein_id_idx using btree ON gene_products(protein_id);
CREATE INDEX gene_products__taxon_id_idx using btree ON gene_products(taxon_id);
CREATE INDEX gene_products__fragment_guid_idx using btree ON gene_products(fragment_guid);
CREATE INDEX gene_products__tagid_idx using btree ON gene_products(tagid);




CREATE TABLE gene_product_synonyms (
    gene_product_guid character varying(255) NOT NULL,
    synonym character varying(80) DEFAULT '' NOT NULL,
    product mediumtext,
    note mediumtext,
    
    FOREIGN KEY (gene_product_guid) REFERENCES gene_products(guid) ON DELETE CASCADE
);
CREATE INDEX gene_product_synonyms__gene_product_guid_idx using btree ON gene_product_synonyms(gene_product_guid);


CREATE TABLE product_annotations (
    gene_product_guid character varying(255) NOT NULL,
    dbreference character varying(20) DEFAULT '' NOT NULL,
    identifier character varying(80) DEFAULT 'unknown' NOT NULL,
    
    FOREIGN KEY (gene_product_guid) REFERENCES gene_products(guid) ON DELETE CASCADE
);
CREATE INDEX product_annotations__gene_product_guid_idx using btree ON product_annotations(gene_product_guid);




CREATE TABLE gene_synonyms (
	id INT NOT NULL AUTO_INCREMENT UNIQUE,
    gene_guid character varying(255) NOT NULL,
    synonym_type character varying(80) DEFAULT '' NOT NULL,
    synonym character varying(80) DEFAULT '' NOT NULL,
    
    FOREIGN KEY (gene_guid) REFERENCES genes(guid) ON DELETE CASCADE
);
CREATE INDEX gene_synonyms__gene_guid_idx using btree ON gene_synonyms(gene_guid);














