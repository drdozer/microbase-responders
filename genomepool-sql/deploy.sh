#!/bin/bash

bucket="software"
path="genome-pool"
name="GenomePool.war"
warPath="./responder/target/genomepool-responder-APT-2.0.war"

mb-deploy-responder.sh ProteinDbResponder $bucket $path $name $warPath 
mb-deploy-responder.sh GenomePoolResponder $bucket $path $name  $warPath


