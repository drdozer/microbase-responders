/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 21, 2012, 2:20:15 PM
 */

package uk.ac.ncl.mucosa.genomepool.ws.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Keith Flanagan
 */
public class DNALocatedPagedItems<T>
    implements Serializable
{
  private int totalItems;
  private float zoomLevel;
  private int totalItemsAtZoomLevel;
  private List<T> items;
  
  public DNALocatedPagedItems()
  {
    items = new ArrayList<T>();
  }

  public List<T> getItems()
  {
    return items;
  }

  public void setItems(List<T> items)
  {
    this.items = items;
  }

  public int getTotalItems()
  {
    return totalItems;
  }

  public void setTotalItems(int totalItems)
  {
    this.totalItems = totalItems;
  }
 
 
}
