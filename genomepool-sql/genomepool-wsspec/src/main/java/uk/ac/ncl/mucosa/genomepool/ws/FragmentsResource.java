/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 21, 2012, 1:41:06 PM
 */

package uk.ac.ncl.mucosa.genomepool.ws;

import org.restlet.resource.Get;
import uk.ac.ncl.mucosa.genomepool.ws.data.GenomeFragmentWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.PagedItems;

/**
 * 
 * @author Keith Flanagan
 */
public interface FragmentsResource
{
  public static enum Parameters {
    ID ("id"),
    TAXON_ID ("taxon_id");
    
    private final String paramName;

    Parameters(String paramName)
    {
      this.paramName = paramName;
    }

    public String paramName()
    {
      return paramName;
    }
  };
  
  /**
   * Returns a pageable list of GenomeFragmentWs objects. Supports parameters:
   *   * offset, limit - returns a specified page of results
   *   * taxon_id - restricts the set of fragments to be returned to the 
   *     specified taxon or taxon(s). Multiple taxons may be specified by
   *     passing multiple 'taxon_id' entries in the request.
   *     If one or more 'taxon_id' entries is passed, then 'offset' and 'limit'
   *     apply within the applicable subset of GenomeFragments.
   *        
   * @return
   * @throws GenomePoolWsException 
   */
  @Get
  public PagedItems<GenomeFragmentWs> getFragments()
      throws GenomePoolWsException;
}
