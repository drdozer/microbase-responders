/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 21, 2012, 9:52:10 PM
 */
package uk.ac.ncl.mucosa.genomepool.ws;

/**
 *
 * @author Keith Flanagan
 */
public enum RESTParams
{

  OFFSET("offset"),
  LIMIT("limit"),
  MAX("max"),
  MIN("min"),
  ID("id");
  
  
  private final String paramName;

  RESTParams(String paramName)
  {
    this.paramName = paramName;
  }

  public String paramName()
  {
    return paramName;
  }
}
