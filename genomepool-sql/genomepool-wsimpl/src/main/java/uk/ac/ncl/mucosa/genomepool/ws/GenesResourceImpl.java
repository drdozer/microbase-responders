/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 15, 2012, 11:48:11 PM
 */
package uk.ac.ncl.mucosa.genomepool.ws;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;
import org.restlet.Server;
import org.restlet.data.Form;
import org.restlet.data.Protocol;
import org.restlet.resource.ServerResource;
import uk.ac.ncl.mucosa.genomepool.full.dao.GeneDAO;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomeFragmentDAO;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;
import uk.ac.ncl.mucosa.genomepool.ws.data.DNALocatedPagedItems;
import uk.ac.ncl.mucosa.genomepool.ws.data.GeneWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.GenomeFragmentWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.PagedItems;

/**
 *
 * @author Keith Flanagan
 */
public class GenesResourceImpl
    extends ServerResource
    implements GenesResource
{
  private static final Logger logger =
      Logger.getLogger(GenesResourceImpl.class.getSimpleName());
  
  
  private static final int DEFAULT_OFFSET = 0;
  private static final int DEFAULT_LIMIT = Integer.MAX_VALUE;
  
  
  private final DistributedJdbcPool gpDbPool;

  public static void main(String[] args)
      throws Exception
  {
    // Create the HTTP server and listen on port 8182  
    new Server(Protocol.HTTP, 8182, GenesResourceImpl.class).start();
  }

  public GenesResourceImpl()
      throws GenomePoolException, Exception
  {
    try
    {
      /*
       * NOTE: This line is *only* required because Tomcat fails to load the 
       * driver in the proper way. If not using Tomcat, remove...
       */
      Class.forName("com.mysql.jdbc.Driver").newInstance(); 
      
      gpDbPool = GenomePoolConfig.createDataSourceFromDefaultConfigFile();
    }
    catch (GenomePoolException ex)
    {
      ex.printStackTrace();
      throw ex;
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      throw ex;
    }
  }
  
  


  @Override
  public DNALocatedPagedItems<GeneWs> getGenes()
      throws GenomePoolWsException
  {
    Form form = getRequest().getResourceRef().getQueryAsForm();
//    int offset = ParamParser.parseOffset(form, DEFAULT_OFFSET);
//    int limit = ParamParser.parseLimit(form, DEFAULT_LIMIT);
    String fragmentGuid = form.getFirstValue(Parameters.FRAGMENT_GUID.paramName());
    int startPos = 0;
    int endPos = Integer.MAX_VALUE;
    
    if (fragmentGuid == null)
    {
      logger.info("Fragment GUID was NULL (or not passed as a paramter)");
    }
    
    Connection txn = null;
    try
    {
      logger.info("Requesting genes for fragment: "+fragmentGuid
          + ", pos: "+startPos+" --> "+endPos);
      txn = gpDbPool.getReadOnlyConnection();
      GenomeFragmentDAO fragDao = new GenomeFragmentDAO();
      GeneDAO geneDao = new GeneDAO();
      
      DNALocatedPagedItems<GeneWs> result = new DNALocatedPagedItems<GeneWs>();
//      result.setOffset(offset);
//      result.setLimit(limit);
      
      
      List<Gene> genes = geneDao.
          getGenesOverlappingCoordinates(txn, fragmentGuid, startPos, endPos);
      List<GeneWs> genesWs = DbToWsConverters.geneDbToWs(genes);
      result.setItems(genesWs);
      
      return result;
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new GenomePoolWsException(
          "Database operation failed: "+e.getMessage(), e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }


}
