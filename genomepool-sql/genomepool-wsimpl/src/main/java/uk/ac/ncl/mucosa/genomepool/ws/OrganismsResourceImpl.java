/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 15, 2012, 11:48:11 PM
 */
package uk.ac.ncl.mucosa.genomepool.ws;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.restlet.Server;
import org.restlet.data.Form;
import org.restlet.data.Protocol;
import org.restlet.resource.ServerResource;
import uk.ac.ncl.mucosa.genomepool.full.dao.OrganismDAO;
import uk.ac.ncl.mucosa.genomepool.ws.data.OrganismWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.PagedItems;

/**
 *
 * @author Keith Flanagan
 */
public class OrganismsResourceImpl
    extends ServerResource
    implements OrganismsResource
{
  private static final int DEFAULT_OFFSET = 0;
  private static final int DEFAULT_LIMIT = Integer.MAX_VALUE;
  
  private final DistributedJdbcPool gpDbPool;

  public static void main(String[] args)
      throws Exception
  {
    // Create the HTTP server and listen on port 8182  
    new Server(Protocol.HTTP, 8182, OrganismsResourceImpl.class).start();
  }

  public OrganismsResourceImpl()
      throws GenomePoolException, Exception
  {
    try
    {
      /*
       * NOTE: This line is *only* required because Tomcat fails to load the 
       * driver in the proper way. If not using Tomcat, remove...
       */
      Class.forName("com.mysql.jdbc.Driver").newInstance(); 
      
      gpDbPool = GenomePoolConfig.createDataSourceFromDefaultConfigFile();
    }
    catch (GenomePoolException ex)
    {
      ex.printStackTrace();
      throw ex;
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      throw ex;
    }
  }
  
  

//  @Get
//  public String blah()
//  {
//    StringBuilder sb = new StringBuilder();
//    Form form = getRequest().getResourceRef().getQueryAsForm();
//    for (Parameter parameter : form)
//    {
//      sb.append("parameter " + parameter.getName());
//      sb.append("/" + parameter.getValue() + "\n");
//    }
//    sb.append("\nCookies:");
//    
//    for (Cookie cookie : getRequest().getCookies())
//    {
//      sb.append("\nname = " + cookie.getName());
//      sb.append("\nvalue = " + cookie.getValue());
//      sb.append("\ndomain = " + cookie.getDomain());
//      sb.append("\npath = " + cookie.getPath());
//      sb.append("\nversion = " + cookie.getVersion());
//      sb.append("\n---------");
//    }
//
//    sb.append("\n\nResource URI  : " + getReference() + '\n' + "Root URI      : "
//        + getRootRef() + '\n' + "Routed part   : "
//        + getReference().getBaseRef() + '\n' + "Remaining part: "
//        + getReference().getRemainingPart()
//        + "\nRequest attributes: " + getRequestAttributes()
//        + "\nResponse attributes: " + getResponseAttributes());
//    return sb.toString();
//  }
//


  @Override
  public PagedItems<OrganismWs> getOrganisms()
      throws GenomePoolWsException
  {
    Form form = getRequest().getResourceRef().getQueryAsForm();
    int offset = ParamParser.parseOffset(form, DEFAULT_OFFSET);
    int limit = ParamParser.parseLimit(form, DEFAULT_LIMIT);
    
    Connection txn = null;
    try
    {
      txn = gpDbPool.getReadOnlyConnection();
      OrganismDAO dao = new OrganismDAO();
      
      PagedItems<OrganismWs> result = new PagedItems<OrganismWs>();
      
      //Get global data info
      int totalOrganisms = dao.countDistinctTaxonIds(txn);
      result.setTotalItems(totalOrganisms);
      
      
      //Get page data for request
      Map<Integer, String> taxonToName = 
          dao.getTaxonToOrganismNames(txn, offset, limit);
      List<OrganismWs> organisms = result.getItems();
      for(int taxonId : taxonToName.keySet())
      {
        String orgName = taxonToName.get(taxonId);
        organisms.add(new OrganismWs(orgName, taxonId));
      }
      
      Collections.sort(organisms, new Comparator<OrganismWs>() {
        @Override
        public int compare(OrganismWs t, OrganismWs t1)
        {
          return t.getName().compareTo(t1.getName());
        }
      });
      
      return result;
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new GenomePoolWsException(
          "Database operation failed: "+e.getMessage(), e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

}
