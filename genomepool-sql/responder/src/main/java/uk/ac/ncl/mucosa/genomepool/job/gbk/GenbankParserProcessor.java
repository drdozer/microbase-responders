/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 9, 2012, 9:07:59 PM
 */

package uk.ac.ncl.mucosa.genomepool.job.gbk;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.io.*;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomeFragmentInserter;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomePoolDAO;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneSynonym;
import uk.ac.ncl.mucosa.genomepool.notification.GbkEntryAvailableMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.GbkEntryFailureMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NewGbkFileMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NotificationConstants;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolConfig;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.util.ExceptionUtils;
import uk.org.microbase.util.file.FileUtils;

/**
 * A Microbase job implementation that takes a Genbank-format file as input and
 * parses the content to the Genome Pool database. In addition, two output
 * resources are created containing the DNA and protein sequences in FASTA
 * format.
 *
 * @author Sirintra Nakjang
 * @author Keith Flanagan
 */
public class GenbankParserProcessor
    extends AbstractMessageProcessor
{
  private static final Logger l =
      Logger.getLogger(GenbankParserProcessor.class.getName());
  
  private String generateOutputPath(String messageId)
  {
     StringBuilder outputPath = new StringBuilder();
     outputPath.append(getResponderInfo().getResponderId());
     outputPath.append("/");
     outputPath.append(messageId);
     return outputPath.toString();
  }
  
  /**
   * Returns a bucket name to use for storing an output file. Usually,
   * the bucket name returned is the same as <code>specifiedInMessage</code>.
   * However, in the case that <code>resultBucketOverride</code> is set, then
   * the overridden location is returned instead.
   * @param specifiedInMessage a bucket name to use by default (usually
   * specified by the incoming message).
   */
  private String determineResultBucket(String specifiedInMessage)
  {
    if (resultBucketOverride != null)
    {
      return resultBucketOverride;
    }
    return specifiedInMessage;
  }
  
  
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    
    // Identification of this responder
    info.setResponderName("GenomePool-gbk");
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    
    // The type of message that this responder is interested in
    info.setTopicOfInterest(NotificationConstants.IN_TOPIC_GBK_FOUND);
    
    // Rate limiting configuration
    info.setEnabled(true);           //Microbase will run this responder
    info.setMaxDistributedCores(30); //Max threads globally
    info.setMaxLocalCores(30);       //Max threads on this machine
    info.setPreferredCpus(1);        //Desired threads per process
    info.setQueuePopulatorRunEveryMs(5 * 1000); //When to check for new messages

    return info;
  }

  private String resultBucketOverride;
  private DistributedJdbcPool gpDbPool;
  private GenomePoolDAO dao;
  
  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    try
    {
      gpDbPool = GenomePoolConfig.createDataSourceFromDefaultConfigFile();
      dao = new GenomePoolDAO();
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed the preRun stage while configuring a database connection pool.", e);
    }
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    /*
     * Perform checks to make sure result database(s) are in a consistent
     * state before processing the message. 
     * 
     * In this case, we simply delete an entry from a table that maps
     * workflow exe and step IDs -> data entries.
     * 
     * A database cascade delete should remove any previously-inserted rows
     * in any table relating to a previous processing attempt of this message.
     */
    Connection txn = null;
    try
    {
      txn = gpDbPool.getWritableConnection();
      dao.removeFragmentByMessage(txn, message.getGuid());
      txn.commit();
    }
    catch (Exception e)
    {
      JdbcUtils.silentRollback(txn);
      /*
       * Throwing a JobProcessingException here informs microbase that the job
       * execution has failed. In this case, Microbase will retry executing this
       * job again at some point in the future.
       */
      throw new ProcessingException(
          "Failed the cleanup stage while processing message: "
          + message.getGuid(), e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public Set<Message> processMessage(Message message)
      throws ProcessingException
  {
    Connection txn = null;
    try
    {
      // Decode message content to GenomePool-specific data bean
      NewGbkFileMessageParser gbkMsg = new NewGbkFileMessageParser(message);
      
      // Get a reference to the temporary directory for this job
      File tmpDir = getWorkingDirectory();
      
      //Download GBK file
      File inputFile = new File(tmpDir, gbkMsg.getName());
      
      getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
          gbkMsg.getBucket(),
          gbkMsg.getPath(),
          gbkMsg.getName(),
          inputFile, true);
      
      //Parse GBK data
      FileInfo sourceFileInfo = new FileInfo(
          gbkMsg.getBucket(),
          gbkMsg.getPath(),
          gbkMsg.getName());
      
      /*
       * A set of messages - one for each entry procecssed - to be published
       * when the entire input has been processed.
       */
      Set<Message> toSend = new HashSet<Message>();
      
      /*
       * Obtain a BufferedReader for the entire input file (i.e., may be more
       * than one Genbank entry)
       */
      BufferedReader inputFileReader = openInputFile(inputFile);
      int entryIndex = 0;
      BufferedReader ramEntryReader;
      
      txn = gpDbPool.getWritableConnection();
      while ((ramEntryReader = 
          GenbankEntryReader.readNextEntryAndSanitize(inputFileReader)) != null)
      {
        try
        {
          l.log(Level.INFO, "Processing GBK entry: {0} from: {1}", 
              new Object[]{entryIndex, sourceFileInfo.toString()});
          /*
          * Parses and stores an individual entry of the input file.
          * If the entry is parsed successfully, then a notification message
          * is fired.
          */
          GbkParsedData gbkData = parseEntry(gbkMsg, ramEntryReader, entryIndex);
          
          l.info("Performing database insert");
          dao.insert(txn, message.getGuid(), message.getWorkflowExeId(), 
              message.getWorkflowStepId(), 
              gbkMsg.getBucket(), gbkMsg.getPath(), gbkMsg.getName(),
              gbkData.getGenomeFragment(),
              gbkData.getGenes(),
              gbkData.getDnaSequence());
          l.info("Database insert completed");
          
          toSend.add(generateSuccessMessage(gbkMsg, entryIndex, gbkData));
        }
        catch(GenomePoolException e)
        {
          l.log(Level.INFO, "Failed to process entry: {0}", entryIndex);
          e.printStackTrace();
          toSend.add(generateFailureMessage(gbkMsg, entryIndex, e));
        }
        finally
        {
          entryIndex++;
        }
      }
      inputFileReader.close();
      
      // Save sequence(s)
      l.info("Committing database transaction");
      txn.commit();
            
      //Finally, send 'success' and 'failure' messages
      return toSend;
    }
    catch (Exception e)
    {
      JdbcUtils.silentRollback(txn);
      /*
       * Throwing a JobProcessingException here informs microbase that the job
       * execution has failed. In this case, Microbase will retry executing this
       * job again at some point in the future.
       */
      throw new ProcessingException(
          "Failed the processMessage stage while attempting to parse a "
          + "GenBank-format file. The message failed to complete for an "
          + "unexpected reason. Message ID: "+message.getGuid(), e);
    }
    finally 
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  private BufferedReader openInputFile(File inputFile)
      throws IOException
  {
    try
    {
      //Attempt to guess whether file needs to be unzipped
      InputStream stream;
      if (inputFile.getName().endsWith(".gz"))
      {
        stream = new GZIPInputStream(new FileInputStream(inputFile));
      }
      else
      {
        stream = new FileInputStream(inputFile);
      }
      
      BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
      return reader;
    }
    catch(Exception e)
    {
      throw new IOException(
          "Failed to open file: "+inputFile.getAbsolutePath(), e);
    }
  }
  
  private GbkParsedData parseEntry(NewGbkFileMessageParser gbkMsgInfo, 
      BufferedReader ramEntryReader, int entryCount)
      throws GenomePoolException
  {
    //Original incoming message
    Message message = gbkMsgInfo.getMessage();
    
    l.log(Level.INFO, "Starting to parse: {0}, entry: {1}", 
        new Object[]{gbkMsgInfo.getName(), entryCount});
    
    /*
     * Generate result data objects and files from input data source
     */
    File dnaFastaFile, proteinFastaFile;
    GbkParsedData gbkData;
    try
    {
      gbkData = Ncbi2Store.parse(ramEntryReader);
      
      //Define two output files:
      dnaFastaFile = new File(getWorkingDirectory(), 
          gbkData.getGenomeFragment().getAccession()+".dna.fasta");
      proteinFastaFile = new File(getWorkingDirectory(), 
          gbkData.getGenomeFragment().getAccession()+".protein.fasta");
      
      // Write a fragment DNA and protein FASTA file
      FastaUtils.writeDnaFastaFile(gbkData, dnaFastaFile);
      FastaUtils.writeProteinFastaFile(gbkData, proteinFastaFile);
    }
    catch(Exception e)
    { 
      throw new GenomePoolException("Failed to parse GenBank entry and store"
          + "it in a database, OR "
          + "failed to write the DNA/Protein FASTA sequence output files."
          + "Input file: "+gbkMsgInfo.getBucket()+", "
          + gbkMsgInfo.getPath()+", "+gbkMsgInfo.getName()
          + ", entry: "+entryCount, e);
    }
    

    /*
     * Upload FASTA output files
     */
    String outputBucket = determineResultBucket(gbkMsgInfo.getBucket());
    String outputPath = generateOutputPath(gbkMsgInfo.getMessage().getGuid());
    try
    {
      getRuntime().getMicrobaseFS().upload(dnaFastaFile, 
          outputBucket, outputPath, dnaFastaFile.getName(), null);
      getRuntime().getMicrobaseFS().upload(proteinFastaFile, 
          outputBucket, outputPath, proteinFastaFile.getName(), null);
    }
    catch(Exception e)
    {
      throw new GenomePoolException("Failed to upload the DNA/Protein FASTA "
          + "sequence output files to the destination bucket: "
          + "Input file: "+gbkMsgInfo.getBucket()+", "
          + gbkMsgInfo.getPath()+", "+gbkMsgInfo.getName()
          + ", entry: "+entryCount, e);
    }
    
    return gbkData;
  }
  
  private Message generateSuccessMessage(
      NewGbkFileMessageParser gbkMsgInfo, int entryIndex, GbkParsedData gbkData)
  {
    Message parentMsg = gbkMsgInfo.getMessage();

    // Prepare a notification message
    Message successMessage = createMessage(
        parentMsg,                                  // The parent message
        NotificationConstants.OUT_TOPIC_GBK_PARSED, // New message topic
        gbkMsgInfo.getName(),                       // Step ID
        "Completed!");                              // Human readable comment
    // Add GenomePool-specific information to the message
    GbkEntryAvailableMessageParser msgDetails = 
        new GbkEntryAvailableMessageParser(successMessage);
    msgDetails.setAccessionNo(gbkData.getGenomeFragment().getAccession());
    msgDetails.setDescription(gbkData.getGenomeFragment().getDescription());
    msgDetails.setFragmentGi(gbkData.getGenomeFragment().getGiNumber());
    msgDetails.setFragmentId(gbkData.getGenomeFragment().getGuid());
    msgDetails.setFragmentType(gbkData.getGenomeFragment().getFragmentType());
    msgDetails.setOrganismName(gbkData.getGenomeFragment().getOrganismName());
    msgDetails.setParsedFromBucket(gbkMsgInfo.getBucket());
    msgDetails.setParsedFromPath(gbkMsgInfo.getPath());
    msgDetails.setParsedFromName(gbkMsgInfo.getName());
    msgDetails.setParsedFromIndex(String.valueOf(entryIndex));
    msgDetails.setTaxonId(gbkData.getGenomeFragment().getTaxonId());
    msgDetails.setVersionNo(gbkData.getGenomeFragment().getVersion());

    return successMessage;
  }
  
  private Message generateFailureMessage(
      NewGbkFileMessageParser gbkMsgInfo, int entryIndex, Throwable cause)
  {
    Message parentMsg = gbkMsgInfo.getMessage();

    // Prepare a notification message
    Message failureMessage = createMessage(
        parentMsg,                                  // The parent message
        NotificationConstants.OUT_TOPIC_GBK_PARSE_FAILURE, // New message topic
        gbkMsgInfo.getName(),                       // Step ID
        cause.getMessage());                        // Human readable comment
    // Add GenomePool-specific information to the message
    GbkEntryFailureMessageParser msgDetails = 
        new GbkEntryFailureMessageParser(failureMessage);
    msgDetails.setParsedFromBucket(gbkMsgInfo.getBucket());
    msgDetails.setParsedFromPath(gbkMsgInfo.getPath());
    msgDetails.setParsedFromName(gbkMsgInfo.getName());
    msgDetails.setParsedFromIndex(String.valueOf(entryIndex));
    msgDetails.setException(ExceptionUtils.throwableToString(cause));

    return failureMessage;
  }
}
