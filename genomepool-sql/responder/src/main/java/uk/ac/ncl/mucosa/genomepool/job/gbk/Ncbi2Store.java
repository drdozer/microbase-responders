
/*
 * This program may only be used, distributed and/or modified under the terms 
 * of the license found in LICENSE.TXT in the project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package uk.ac.ncl.mucosa.genomepool.job.gbk;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.util.*;
import java.util.regex.*;
import java.io.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;

import java.util.logging.Logger;
import org.biojava.bio.*;
import org.biojava.bio.seq.*;
import org.biojava.bio.seq.io.*;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomePoolDAO;
import uk.ac.ncl.mucosa.genomepool.full.data.FragmentType;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProduct;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProductSynonym;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneSynonym;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;
import uk.ac.ncl.mucosa.genomepool.full.data.ProductAnnotation;
import uk.ac.ncl.mucosa.genomepool.job.ParseDataException;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolConfig;
import uk.org.microbase.filesystem.spi.FileInfo;
import uk.org.microbase.util.UidGenerator;

/**
 * This class is responsible for parsing a flat-file .gbk format file to a set
 * of objects that can be stored in a database.
 *
 * @author Sirintra Nakjang
 * @author Keith Flanagan
 */
public class Ncbi2Store
{
  private static final Logger logger = Logger.getLogger(Ncbi2Store.class.getName());

  //public static ParsedData parse(File gbkFile, String gbkResourceId)
  public static GbkParsedData parse(BufferedReader br)//, FileInfo parsedFrom)
      throws ParseDataException
  {
    try
    {
      //Read NCBI/refseq file
      SequenceIterator stream = SeqIOTools.readGenbank(br);

      //create sequence object from file
      Sequence seq = stream.nextSequence();
      String seqName = seq.getName();
      //setAccNumber(seqName);
      seqName = seqName.replaceAll(";", "");    // Remove all ;

      logger.info("\n" + "FILE : Name : " + seqName); //get sequence ID
      logger.info("\n" + "FILE : Alphabet : " + seq.getAlphabet().getName());
      logger.info("\n" + "FILE : Length : " + seq.length()); //get number of base pairs
      logger.info("\n" + "FILE : Features : " + seq.countFeatures()); //get number of features

      //get DNA sequence
      String dna = seq.seqString();
      //logger.info("\n"+"DNA : "+ dna);

      //create annotation object from sequence
      Annotation seqAnn = seq.getAnnotation();
      Map seqAnnMap = seq.getAnnotation().asMap();


      /*
       * Populate basic Organism and GenomeFragment fields
       */

      String fragmentGi = (String) seqAnnMap.get("GI");
      String fragmentComment = (String) seqAnnMap.get("COMMENT");
      String modifiedDate = (String) seqAnnMap.get("MDAT");
      String fragmentDefn = (String) seqAnnMap.get("DEFINITION");
      String version = (String) seqAnnMap.get("VERSION");
      String genbankTaxonLine = (String) seqAnnMap.get("ORGANISM");


      GbkParsedData data = new GbkParsedData();
      GenomeFragment fragment = data.getGenomeFragment();
      fragment.setGuid(UidGenerator.generateUid());
      List<Gene> genes = data.getGenes();

      //organism.setGenbankTaxonLine(genbankTaxonLine);

      if (fragmentGi != null)
      {
        fragment.setGiNumber(Integer.parseInt(fragmentGi));
        //fragment.setGuid(String.valueOf(fragment.getGiNumber()));
      }
      //fragment.setOrganismGuid("");
      fragment.setComment(fragmentComment);
      fragment.setNcbiModified(modifiedDate);
      fragment.setDescription(fragmentDefn);

      if (seqAnn.containsProperty("SOURCE"))
      {

        String name = concattenate(seqAnn.getProperty("SOURCE"));
        logger.info("\n" + "FILE : name : " + name);
        fragment.setOrganismName(name);

        //get short name for an organism
        Pattern p = Pattern.compile("(\\w{2})(\\w*\\s+)(\\w{2,3})");
        Matcher m = p.matcher(name);
        m.find();
        //logger.info("\n"+m.group());
        String shortName = m.group(1) + m.group(3);
        shortName = shortName.toLowerCase();
        logger.info("\n" + "FILE : shortName : " + shortName);
        fragment.setOrganismShortName(shortName);
      }

      //Protein sequence counter
      //int countProtein = 0;

      //fixed bugs where 'FEATURE' has more than 1 'source'
      int num_source = 0;

      //For FT lines

      for (Iterator fi = seq.features(); fi.hasNext();)
      {
        Feature f = (Feature) fi.next();
        FragmentType fragmentType = FragmentType.unknown;
        //String fragmentType = "unknown";
        String fragment_subtype = "unspecified";

        //get source information
        if (f.getType().equals("source") && num_source == 0)
        {
          num_source++;
          //continue;
          Annotation ann = f.getAnnotation();

          //check fragment type
          if (ann.containsProperty("chromosome"))
          {
            //fragmentType = "chromosome";
            fragmentType = FragmentType.chromosome;
            fragment_subtype = (String) ann.getProperty("chromosome");
          }
          else if (ann.containsProperty("plasmid"))
          {
            //fragmentType = "plasmid";
            fragmentType = FragmentType.plasmid;
            fragment_subtype = (String) ann.getProperty("plasmid");
          }
          else if (ann.containsProperty("organelle"))
          {
            //fragmentType = "organelle";
            fragmentType = FragmentType.organelle;
            fragment_subtype = (String) ann.getProperty("organelle");
          }
          //else if (fragmentType.equals("unknown"))
          else if (fragmentType == FragmentType.unknown)
          {
            Pattern p = Pattern.compile("complete genome");
            Matcher m = p.matcher(fragment.getDescription());
            if (m.find())
            {
              //logger.info("\n"+m.group());
              fragmentType = FragmentType.genome;
              //fragmentType = "genome";
              fragment_subtype = "complete genome";
            }
          }

          logger.info("\n" + "fragment_type: " + fragmentType);
          logger.info("\n" + "fragment_subtype: " + fragment_subtype);

          String taxon = null;
          List<String> refs = new ArrayList<String>();
          if (ann.containsProperty("db_xref"))
          {
            //taxon = (String) ann.getProperty("db_xref");
            //ArrayList<String>refs = (ArrayList) ann.getProperty("db_xref");
            refs = itemise(ann.getProperty("db_xref"));
            for (String s : refs)
            {
              Pattern patt = Pattern.compile("(\\w*)(:)(\\w*)");
              Matcher match = patt.matcher(s);
              if (match.find() && (match.group(1)).equals("taxon"))
              {
                taxon = match.group(3);
              }
            }
            logger.info("\n" + "taxon: " + taxon);
          }

          if (taxon == null)
          {
            throw new ParseDataException("Organism has no taxon id!");
          }

          int taxonint = Integer.parseInt(taxon);
          fragment.setTaxonId(taxonint);

          fragment.setAccession(seqName);
          fragment.setVersion(version);
          fragment.setFragmentType(fragmentType);
          fragment.setFragmentSubtype(fragment_subtype);
          //fragment.setFileid(gbkResourceId);
//          fragment.setParsedFrom(parsedFrom);

          fragment.setDnaSequenceLength(dna.length());

          data.setDnaSequence(dna);

        }


        //get CDS information
        if (!f.getType().equals("CDS"))
        {
          continue;
        }

        int start_codon = f.getLocation().getMin(); //get start codon
        int stop_codon = f.getLocation().getMax(); //get stop coden

        //get strand orientation
        StrandedFeature sf = (StrandedFeature) f;
        char strand = sf.getStrand().getToken();
        logger.info("\n" + "FILE : Location : " + f.getType() + "\t" + strand + "\t" + f.getLocation() + "\t" + start_codon + "-" + stop_codon);


        Annotation ann = f.getAnnotation();
        //get gene_id (EMBL gene id)
        String geneID = null;

        //get db_xref (contains GeneID)
        //ArrayList<String> xref = new ArrayList<String>();
        List<String> xref = new ArrayList<String>();
        if (ann.containsProperty("db_xref"))
        {
          //xref= (ArrayList)ann.getProperty("db_xref");
          xref = itemise(ann.getProperty("db_xref"));
        }
        String dbRef = null;
        String identifier = null;
        for (String s : xref)
        {
          String ss = s;
          Pattern patt = Pattern.compile("(\\w*)(:)(\\w*)");
          Matcher match = patt.matcher(ss);
          if (match.find())
          {
            dbRef = match.group(1);
            identifier = match.group(3);
          }

          if (dbRef.equals("GeneID"))
          {
            geneID = identifier;
          }

          logger.info("\n" + "FILE : xref: " + dbRef + "\t" + identifier);
        }

        logger.info("\n" + "FILE : geneID : " + geneID);
        //get locus-tag (locus/gene name and Uniport ID)
        String locusTag = null;
        //String uniprotID = null;
        if (ann.containsProperty("locus_tag"))
        {
          locusTag = (String) ann.getProperty("locus_tag");
        }
        logger.info("\n" + "FILE : LocusTag : " + locusTag);

        //get geneName
        String geneName = null;
        if (ann.containsProperty("gene"))
        {
          geneName = (String) ann.getProperty("gene");
        }
        logger.info("\n" + "FILE : Gene : " + geneName);

        Gene gene = new Gene();
        gene.setGuid(UidGenerator.generateUid());
        gene.setFragmentGuid(fragment.getGuid());
        gene.setTaxonId(fragment.getTaxonId());

//        if (gene.getOrganismGuid() == null)
//        {
//          throw new RuntimeException("organism id is null");
//        }
//        if (gene.getFragmentGuid() == null)
//        {
//          throw new RuntimeException("fragment id is null");
//        }
        //gene.setGenomeFragment(fragment);
        genes.add(gene);
        gene.setStartLocus(start_codon);
        gene.setEndLocus(stop_codon);
        gene.setOrientation(String.valueOf(strand));

        if (geneID != null)
        {
          if (gene.getGeneId() == null)
          {
            //If there are multiple values, only set the first one
            gene.setGeneId(geneID);
            //gene.setGuid(geneID);
          }

          GeneSynonym synonym = new GeneSynonym();
          //synonym.setGene(gene);
          synonym.setGeneGuid(gene.getGuid());
          gene.getSynonyms().add(synonym);
          synonym.setSynonymType(GeneSynonym.SynonymType.GENE_ID);
          synonym.setSynonym(geneID);
        }

        if (locusTag != null)
        {
          if (gene.getLocusTag() == null)
          {
            //If there are multiple values, only set the first one
            gene.setLocusTag(locusTag);
          }

          GeneSynonym synonym = new GeneSynonym();
          //synonym.setGene(gene);
          synonym.setGeneGuid(gene.getGuid());
          gene.getSynonyms().add(synonym);
          synonym.setSynonymType(GeneSynonym.SynonymType.LOCUS_TAG);
          synonym.setSynonym(locusTag);
        }

        if (geneName != null)
        {
          if (gene.getGeneName() == null)
          {
            //If there are multiple values, only set the first one
            gene.setGeneName(geneName);
          }

          GeneSynonym synonym = new GeneSynonym();
          //synonym.setGene(gene);
          synonym.setGeneGuid(gene.getGuid());
          gene.getSynonyms().add(synonym);
          synonym.setSynonymType(GeneSynonym.SynonymType.GENE_NAME);
          synonym.setSynonym(geneName);
        }

        //get protein_id
        String proteinID = null;
        if (ann.containsProperty("protein_id"))
        {
          proteinID = (String) ann.getProperty("protein_id");
          Pattern patt = Pattern.compile("(.*)(\\s+\\{)");
          Matcher match = patt.matcher(proteinID);
          if (match.find())
          {
            proteinID = match.group(1);
          }
        }
        logger.info("\n" + "ProteinID : " + proteinID);

        //get product_description
        String product_desc = null;
        String cds_note = null;
        if (ann.containsProperty("product"))
        {
          product_desc = (String) ann.getProperty("product");

        }
        logger.info("\n" + "Product_desc : " + product_desc);

        if (ann.containsProperty("note"))
        {
          cds_note = (String) ann.getProperty("note");

        }
        logger.info("\n" + "CDS_note : " + cds_note);


        //get protein sequece
        //String translation = "NO_DATA";
        String translation = null;
        if (ann.containsProperty("translation"))
        {
          translation = (String) ann.getProperty("translation");

          translation.replaceAll("\\s+$", "");
          translation.trim();
        }
        else if (proteinID != null)
        {
          /*
           * Attempts a download from NCBI if no sequence exists in the .gbk file.
           * Perhaps useful for incomplete .gbk files?
           * Leaving this in fow now
           */
          String fetchSeq = fetchSeqeunce(proteinID);
          if (fetchSeq.length() != 0)
          {
            translation = fetchSeq;
          }
        }
        logger.info("\n" + "FILE : Translation: " + translation);


        //get product information
        String productType = "protein";

        //generate protein unique id (UID) from file
        char readstrand = 'd';
        if (strand == '-')
        {
          readstrand = 'c';
        }
        //add protein id to UID to make it unique in case of join-CDS protein sequence with same start-stop positions
        //NC_003212.1CDSd1119735_1121381PROTIDNP_470435.1
        String UID = version + f.getType() + readstrand + start_codon + "_" + stop_codon + "PROTID" + proteinID;

        GeneProduct product = new GeneProduct();
        product.setGuid(UidGenerator.generateUid());
        product.setGeneGuid(gene.getGuid());
        gene.getProducts().add(product);
        product.setTaxonId(fragment.getTaxonId());
        product.setProductType(productType);
        product.setProductSequence(translation);
        product.setFragmentGuid(fragment.getGuid());
        product.setProteinTagId(UID);
        product.setProteinId(proteinID);
        product.setProductDescr(product_desc);

        if (proteinID != null)
        {
          GeneProductSynonym synonym = new GeneProductSynonym();
          //synonym.setGeneProduct(product);
          synonym.setGeneProductGuid(product.getGuid());
          product.getSynonyms().add(synonym);
          synonym.setSynonym(proteinID);
          synonym.setProduct(product_desc);
          synonym.setNote(cds_note);
        }

        //get EC_number
        List<String> ecs = new ArrayList<String>();
        String ec_number = null;
        if (ann.containsProperty("EC_number"))
        {
          ecs = itemise(ann.getProperty("EC_number"));
          //ec_number = (String) ann.getProperty("EC_number");
          for (String s : ecs)
          {
            ec_number = s;

            ProductAnnotation annotation = new ProductAnnotation();
            //annotation.setGeneProduct(product);
            annotation.setGeneProductGuid(product.getGuid());
            product.getAnnotations().add(annotation);
            annotation.setDbReference("EC number");
            annotation.setIdentifier(ec_number);
          }
        }

        //get db_xref (contains GeneID)
        //ArrayList<String> xrefs = new ArrayList<String>();
        List<String> xrefs = new ArrayList<String>();
        if (ann.containsProperty("db_xref"))
        {
          //xrefs= (ArrayList)ann.getProperty("db_xref");
          xrefs = itemise(ann.getProperty("db_xref"));
        }
        String dbRef_p = null;
        String identifier_p = null;
        for (String s : xrefs)
        {
          String ss = s;
          Pattern patt = Pattern.compile("(\\w*)(:)(\\w*)");
          Matcher match = patt.matcher(ss);
          if (match.find())
          {
            dbRef_p = match.group(1);
            identifier_p = match.group(3);
          }

          ProductAnnotation annotation = new ProductAnnotation();
          //annotation.setGeneProduct(product);
          annotation.setGeneProductGuid(product.getGuid());
          product.getAnnotations().add(annotation);
          annotation.setDbReference(dbRef_p);
          annotation.setIdentifier(identifier_p);

          //Also set 'gi' on the GeneProduct itself
          if (dbRef_p.equals("GI"))
          {
            int gi = Integer.parseInt(identifier_p);
            product.setGi(gi);
//            product.setGuid(String.valueOf(gi));
          }
        }
        if (product.getGuid() == null)
        {
          StringBuilder msg = new StringBuilder();
          msg.append("Failed to parse product with a null GUID - possibly no ")
              .append("GI entry - check your .gbk file consistency:")
//              .append("\n * File: ").append(parsedFrom)
              .append("\n * Fragment GI: ").append(fragmentGi)
              .append("\n * Fragment Version: ").append(version)
              .append("\n * Fragment definition: ").append(fragmentDefn)
              .append("\n * Fragment version: ").append(version)
              .append("\n * Product fragment GUID: ").append(product.getFragmentGuid())
              .append("\n * Product gene GUID: ").append(product.getGeneGuid())
              .append("\n * Product protein ID: ").append(product.getProteinId())
              .append("\n * Product protein tag ID: ").append(product.getProteinTagId())
              .append("\n * Product type: ").append(product.getProductType())
              .append("\n * Product descr: ").append(product.getProductDescr())
              .append("\n * Product seq: ").append(product.getProductSequence())
              .append("\n");


          logger.info(msg.toString());
          throw new ParseDataException(msg.toString());
        }
        data.getProductGuidToTranslation().put(
          product.getGuid(), product.getProductSequence());
      }

      /*
       * Post-processing... set Gene and GeneProduct organismGuid and fragmentGuid
       * fields, since many of these might still be null (because we can't
       * determine the order of feature iterations and therefore might not have
       * read the taxon id first).
       * Messy fix, but works for now
       */
      for (Gene gene : data.getGenes())
      {
        gene.setTaxonId(fragment.getTaxonId());
        gene.setFragmentGuid(fragment.getGuid());
        for (GeneProduct prod : gene.getProducts())
        {
          prod.setTaxonId(fragment.getTaxonId());
          prod.setFragmentGuid(fragment.getGuid());
        }
      }

      //insert total number of proteinsequence of each genome fragment
      fragment.setNumberProtein(genes.size());

      return data;
    }
    catch (java.lang.Exception ex)
    {
      ex.printStackTrace();
      logger.info("\n" + "INSERT_FAILED : NCBI2STORE\n\n");
      throw new ParseDataException("Parsing genome file failed", ex);
    }
    finally
    {
    }
  }

  /**
   **/
  public static String concattenate(Object o)
  {
    if (o == null)
    {
      return null;
    }
    if (o instanceof String)
    {
      return (String) o;
    }
    else if (o instanceof List)
    {
      List<String> items = (List<String>) o;
      StringBuilder sb = new StringBuilder();
      for (String line : items)
      {
        sb.append(line);
      }
      return sb.toString();
    }
    else
    {
      throw new RuntimeException("Unexpected type: " + o.getClass().getName());
    }
  }

  public static List<String> itemise(Object o)
  {
    if (o == null)
    {
      return null;
    }
    if (o instanceof String)
    {
      List<String> items = new ArrayList<String>();
      items.add((String) o);
      return items;
    }
    else if (o instanceof List)
    {
      return (List) o;
    }
    else
    {
      throw new RuntimeException("Unexpected type: " + o.getClass().getName());
    }
  }

  public static String fetchSeqeunce(String id)
      throws MalformedURLException, IOException
  {
    //String BASE_URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&rettype=fasta&id=";
    String BASE_URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&rettype=fasta&id=";

    URL url = new URL(BASE_URL + id);

    System.out.println("Downloading...");
    byte[] completeDownload = download(url);

    InputStream is = new ByteArrayInputStream(completeDownload);
    BufferedReader r = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();
    String line;
    while ((line = r.readLine()) != null)
    {
      if (!line.startsWith(">"))
      {
        sb.append(line);
      }
    }
    String sequence = sb.toString();
    return sequence;

  }

  public static byte[] download(URL url)
  {
    while (true)
    {
      try
      {
        System.out.println("Connecting to: " + url.toString());
        InputStream is = url.openStream();
        System.out.println("Connected!");

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int read;
        System.out.println("Downloading");
        while ((read = is.read(buffer)) != -1)
        {
          os.write(buffer, 0, read);
        }
        is.close();

        byte[] completeDownload = os.toByteArray();
        return completeDownload;
      }
      catch (IOException e)
      {
        e.printStackTrace();
        System.out.println("Trying again later...");
        try
        {
          Thread.sleep(1000 * 60);
        }
        catch (InterruptedException e2)
        {
          e2.printStackTrace();
        }
      }
    }
  }

  /**
   * This program entry point can be used for testing/debuggin the parser or for
   * manually populating a database.
   *
   * @param args
   * @throws Exception
   */
//  public static void main(String[] args)
//      throws Exception
//  {
//    if (args.length == 1)
//    {
//      File gbkFile = new File(args[0]);
//      
//      GbkParsedData data = parse(gbkFile);
//      GenomeFragment fragment = data.getGenomeFragment();
//      List<Gene> genes = data.getGenes();
//
//      System.out.println("\n\n\n-----------------------------------------\n\n\n");
//      System.out.println("\nFragment: " + fragment.toString());
//      System.out.println("\nGenes: " + genes.size());
//      System.out.println("\nNo. translations: "+data.getProductGuidToTranslation().size());
//      System.out.println("\nDNA length: " + data.getDnaSequence().length());
//
//    }
//    else if (args.length == 3)
//    {
//      File gbkDir = new File(args[0]);
//      String dbConfigResource = args[1];
//      int numThreads = Integer.parseInt(args[2]);
//
//      final DistributedJdbcPool gpDbPool = GenomePoolConfig.createDataSourceFromResource(dbConfigResource);
//
//      final GenomePoolDAO dao = new GenomePoolDAO();
//      
//      File[] gbkFiles = gbkDir.listFiles(new FilenameFilter()
//      {
//        @Override
//        public boolean accept(File arg0, String arg1)
//        {
//          return arg1.endsWith(".gbk");
//        }
//      });
//
//      List<File> gbkFileList = new LinkedList<File>();
//      gbkFileList.addAll(Arrays.asList(gbkFiles));
//
//      final Iterator<File> gbkFileListItr = gbkFileList.iterator();
//
//      Set<Thread> threads = new HashSet<Thread>();
//      for (int i=0; i< numThreads; i++)
//      {
//        System.out.println("Starting thread: "+i);
//        Runnable worker = new Runnable() {
//          @Override
//          public void run()
//          {
//            while(true)
//            {
//              File gbkFile = null;
//              synchronized(gbkFileListItr)
//              {
//                if (gbkFileListItr.hasNext())
//                {
//                  gbkFile = gbkFileListItr.next();
//                }
//                else
//                {
//                  return;
//                }
//              }
//              try
//              {
//                Connection txn = null;
//                try
//                {
//                  GbkParsedData data = parse(gbkFile);
//                  txn = gpDbPool.getWritableConnection();
//                  dao.insert(txn, null, 
//                      null, null,
//                      null, null, gbkFile.getName(),
//                      data.getGenomeFragment(),
//                      data.getGenes(),
//                      data.getDnaSequence());
//                  txn.commit();
//                }
//                finally
//                {
//                  JdbcUtils.silentClose(txn);
//                }
//              }
//              catch(Exception e)
//              {
////                e.printStackTrace();
//                logger.severe("Exception while parsing / storing data. "
//                    + "See stack trace that follows for more information.");
//                throw new RuntimeException(
//                    "Something went wrong while parsing / storing data.", e);
//              }
//            }
//
//          }
//        };
//        Thread t = new Thread(worker);
//        t.setDaemon(true);
//        t.start();
//        threads.add(t);
//      }
//
//      for (Thread t : threads)
//      {
//        t.join();
//        System.out.println("Thread "+t.getName()+" has finished");
//      }
//
//      // Old, single-threaded version
////      for (File gbkFile : gbkFiles)
////      {
////        System.out.println("Processing: " + gbkFile.getAbsolutePath());
////        ParsedData data = parse(gbkFile, null);
////        dao.insert(
////            data.getGenomeFragment(),
////            data.getGenes(),
////            data.getDnaSequence());
////      }
//      
//    }
//    else
//    {
//      System.out.println("Usage:");
//      System.out.println("  test");
//      System.out.println("    1) .gbk file\n");
//      System.out.println("  insert");
//      System.out.println("    1) .gbk directory (inserts all files)");
//      System.out.println("    2) classpath resource path to DB config file");
//      System.out.println("    3) no. writing threads");
//      System.exit(1);
//    }
//  }
}
