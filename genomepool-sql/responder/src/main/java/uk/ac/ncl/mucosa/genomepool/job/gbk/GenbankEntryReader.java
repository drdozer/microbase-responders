/*
 * Copyright 2012 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ncl.mucosa.genomepool.job.gbk;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.mucosa.genomepool.job.ParseDataException;
import uk.org.microbase.util.file.FileUtils;

/**
 * A utility to assist when dealing with files containing multiple GenBank
 * formatted entries.
 * 
 * You can use <code>readNextEntry</code> to read GenBank files as-is.
 * Alternatively, you can use <code>readNextEntryAndSanitize</code> to read an
 * entry and then fix common formatting problems, such as missing quotes, etc.
 * 
 * @author Keith Flanagan
 */
public class GenbankEntryReader 
{
  private static final Logger logger = 
      Logger.getLogger(GenbankEntryReader.class.getName());
  
  /**
   * Given a BufferedReader, reads a single GenBank-formatted entry from the stream
   * and into RAM. A BufferedReader is returned that points to a byte[] array
   * containing the entry.
   * 
   * GenBank entries are assumed to be delimited in the standard way with a 
   * single line containing '//'.
   * 
   * @param br a BufferedReader containing the input data. This could potentially
   * be from a massive '.genomic.gbff.gz' containing hundreds of GenBank entries.
   * 
   * @return a BufferedReader containing a complete GenBank entry, or null if
   * no GenBank entry was found (e.g., reached the end of the stream).
   */
  public static BufferedReader readNextEntry(BufferedReader br)
      throws ParseDataException
  {
    try
    {
      ByteArrayOutputStream temp = new ByteArrayOutputStream();
      BufferedWriter tempBw = new BufferedWriter(new OutputStreamWriter(temp));

      int lineCount = 0;
      boolean endOfGbkFound = false;
      for (String line = br.readLine(); line != null; line = br.readLine())
      {
        tempBw.write(line);
        tempBw.write("\n");
        lineCount++;
        
        if (line.trim().equals("//"))
        {
          endOfGbkFound = true;
          logger.log(Level.INFO, 
              "Reached end of GenBank entry. Lines: {0}", lineCount);
          break;
        }
      }

      if (!endOfGbkFound)
      {
        logger.info("Assuming we reached the end of file");
        return null;
      }
      
      BufferedReader ramReader = new BufferedReader(
          new InputStreamReader(new ByteArrayInputStream(temp.toByteArray())));
      return ramReader;
    }
    catch(Exception e)
    {
      throw new ParseDataException(
          "Failed to read a GenBank entry into memory", e);
    }
  }
  
  /**
   * Similar to <code>readNextEntry</code>, but also fixes a common formatting
   * problems found in a number of GenBank entries. 
   * Code originally from Ncbi2Store.
   * 
   * Given a BufferedReader, reads a single GenBank-formatted entry from the stream
   * and into RAM. A BufferedReader is returned that points to a byte[] array
   * containing the entry.
   * 
   * GenBank entries are assumed to be delimited in the standard way with a 
   * single line containing '//'.
   * 
   * @param br a BufferedReader containing the input data. This could potentially
   * be from a massive '.genomic.gbff.gz' containing hundreds of GenBank entries.
   * 
   * @return a BufferedReader containing a complete GenBank entry, or null if
   * no GenBank entry was found (e.g., reached the end of the stream).
   * @throws ParseDataException 
   */
  public static BufferedReader readNextEntryAndSanitize(BufferedReader br)
      throws ParseDataException
  {
    try
    {
      ByteArrayOutputStream temp = new ByteArrayOutputStream();
      BufferedWriter tempBw = new BufferedWriter(new OutputStreamWriter(temp));

      int lineCount = 0;
      boolean endOfGbkFound = false;
      for (String line = br.readLine(); line != null; line = br.readLine())
      {
        /*
         * Fix GenBank formatting problems.
         */
        
        // Fixes bug on new line in quotation marks of CDS \note tags (which 
        // prevents biojava to read through the file properly) 
        line = line.replaceAll("note=\"", "note=\"note: ");

//        //FIX broken GenBank file where quotes appear in note entries
        //line = line.replaceAll("\"$", "\".");
//line = line.replaceAll("\"\\s{1}", "");
//line = line.replaceAll("\\s{1}\"", "");
//        line = line.replaceAll("\\s\"", "");
//        //End fix

        //unusual line found in human chromosome (eg. NC_000001.9, NC_000002.10))))
        line = line.replaceFirst("aa:\\w*\\)", "/aa:OTHER");
        
        /*
         * Write the fixed line to an in-memory buffer
         */
        tempBw.write(line);
        tempBw.write("\n");
        lineCount++;
        
        if (line.trim().equals("//"))
        {
          endOfGbkFound = true;
          logger.log(Level.INFO, 
              "Reached end of GenBank entry. Lines: {0}", lineCount);
          break;
        }
      }
      
      if (!endOfGbkFound)
      {
        logger.info("No '//' line found, assuming that this is the end of "
            + "the file and no more sequences are present.");
        return null;
      }
      
      tempBw.flush();
      tempBw.close();
      
      // *****************************************************************
      // DEBUG: write sanitized output to a file for debugging purposes
//      FileOutputStream tmpFos = new FileOutputStream("tmp.gbk");
//      FileUtils.copyStream(new ByteArrayInputStream(temp.toByteArray()), tmpFos);
//      tmpFos.flush();
//      tmpFos.close();
      // *****************************************************************

      BufferedReader ramReader = new BufferedReader(
          new InputStreamReader(new ByteArrayInputStream(temp.toByteArray())));
      return ramReader;
    }
    catch(Exception e)
    {
      throw new ParseDataException(
          "Failed to read a GenBank entry into memory", e);
    }
  }
}
