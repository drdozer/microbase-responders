/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.cli;

import java.util.Arrays;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import uk.ac.ncl.mucosa.genomepool.notification.NewGbkFileMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NotificationConstants;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolException;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.filesystem.spi.FileInfo;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.UidGenerator;

/**
 * This program connects to a GenomePool instance and instructs it to
 * manually submit .gbk files existing in the Microbase FS for parsing.
 *
 * @author Keith Flanagan
 */
public class ManualSubmitGbk
{
  private static final Logger logger =
      Logger.getLogger(ManualSubmitGbk.class.getName());

  private static ClientRuntime runtime;

  private static final String DEFAULT_RESULT_SET = "test-results";
  
  private static final String DEFAULT_FILE_EXTENSION = ".gbk";

  private static void printHelpExit(Options options)
  {
    printHelpExit(options, null);
  }

  private static void printHelpExit(Options options, String additionalFooter)
  {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "submit_gbk_jobs.sh";
    String header = "";
    StringBuilder footer = new StringBuilder();
    footer.append("Submits one or more GBK files for processing by firing"
        + "an event notification for each, which are later picked up by the"
        + "GenomePool responder.\n\n"
        + "You must either specify a bucket/path/filename combination to "
        + "submit a single file for parsing;\n"
        + "OR, you may specify just bucket/path in order to submit the entire"
        + "directory.\n"
        + "All files submitted for processing must currently exist in the MBFS.");
    if (additionalFooter != null)
    {
      footer.append("\n").append(additionalFooter);
    }
    int width = 80;
    //formatter.printHelp( "notification.sh", options );
    formatter.printHelp(width, cmdSyntax, header, options, footer.toString());
    System.exit(0);
  }


  public static void main(String[] args)
      throws GenomePoolException, FSOperationNotSupportedException, FSException//, ParseException
  {
    CommandLineParser parser = new PosixParser();

    Options options = new Options();

    options.addOption(OptionBuilder
        .withLongOpt("result-set")
        //.withArgName( "ID" )
        .hasArgs(1)
        .withDescription(
        "Specifies a result-set for the current operation "
        + "(default: "+DEFAULT_RESULT_SET+")")
        .create( "r" ));

    options.addOption(OptionBuilder
        .withLongOpt("bucket")
        //.withArgName( "hostname" )
        .hasArgs(1)
        .withDescription(
        "Specifies the MBFS bucket to use ")
        .create( "b" ));

    options.addOption(OptionBuilder
        .withLongOpt("path")
        //.withArgName( "hostname" )
        .hasArgs(1)
        .withDescription(
        "Specifies the MBFS file path to use ")
        .create( "p" ));

    options.addOption(OptionBuilder
        .withLongOpt("filename")
        //.withArgName( "hostname" )
        .hasArgs(1)
        .withDescription(
        "Specifies the MBFS filename to use ")
        .create( "f" ));

    if (args.length == 0)
    {
      printHelpExit(options);
    }

    String resultSet = DEFAULT_RESULT_SET;

    String bucket = null;
    String path = null;
    String filename = null;
    try
    {
      CommandLine line = parser.parse(options, args);

      System.out.println(Arrays.asList(line.getArgs()));
      System.out.println(Arrays.asList(line.getOptions()));

      if (line.hasOption("result-set"))
      {
        resultSet = line.getOptionValue("result-set");
      }
      if (line.hasOption("bucket"))
      {
        bucket = line.getOptionValue("bucket");
      }
      if (line.hasOption("path"))
      {
        path = line.getOptionValue("path");
      }
      if (line.hasOption("filename"))
      {
        filename = line.getOptionValue("filename");
      }

      if (bucket == null || path == null)
      {
        printHelpExit(options, "Bucket and path are required!");
      }
    }
    catch(ParseException e)
    {
      e.printStackTrace();
      printHelpExit(options);
    }
    finally
    {
    }



    //Load Microbase client global configuration from classpath
    try
    {
      System.out.println("Connecting to Microbase");
      ///Configure
      runtime = GlobalConfiguration.getDefaultRuntime();
      System.out.println("Connected: "+runtime.toString());
    }
    catch(Exception e)
    {
      logger.info("Failed to configure a Microbase runtime based on "
          + "settings in the configuration files.");
      e.printStackTrace();
      System.exit(1);
    }

    try
    {
      if (filename == null)
      {
        submitDirectory(resultSet, bucket, path);
      }
      else
      {
        submitFileForProcessing(resultSet, bucket, path, filename);
      }
    }
    finally
    {
      System.out.println("\n\n\nDisconnecting from Microbase...");
      runtime.shutdown();
    }
  }

  private static void submitDirectory(
      String resultSet, String bucket, String path)
      throws FSOperationNotSupportedException, FSException, GenomePoolException
  {

    MicrobaseFS fs = runtime.getMicrobaseFS();
    Set<String> names = fs.listRemoteFilenames(bucket, path, false, false);

    for (String name : names)
    {
      if (name.endsWith(DEFAULT_FILE_EXTENSION))
      {
        System.out.println("Submitting: Name: "+name);
        submitFileForProcessing(resultSet, bucket, path, name);
      }
    }

    logger.log(Level.INFO,
        "Found {0} files at: {1}, {2}", new Object[]{names.size(), bucket, path});
  }

  private static void submitFileForProcessing(
      String resultSet, String bucket, String path, String filename)
      throws GenomePoolException
  {
    System.out.println("Submitting request to webservice for: "+filename);
    FileInfo gbkFile = new FileInfo(bucket, path, filename);
    //TODO - could maybe check that file exists?

    String topic = NotificationConstants.IN_TOPIC_GBK_FOUND;
    String publisher = ManualSubmitGbk.class.getName();

    Message message = new Message();
    message.setGuid(UidGenerator.generateUid());
    message.setTopicGuid(topic);
    message.setPublisherGuid(publisher);
    message.setWorkflowExeId(resultSet);
    message.setWorkflowStepId(filename);

    NewGbkFileMessageParser gbkMsg = new NewGbkFileMessageParser(message);
    gbkMsg.setBucket(bucket);
    gbkMsg.setPath(path);
    gbkMsg.setName(filename);

    try
    {
      runtime.getNotification().publish(message);
    }
    catch(Exception e)
    {
      logger.info("Failed to publish message:");
      e.printStackTrace();
    }


  }
}
