/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-May-2012, 22:36:58
 */
package uk.ac.ncl.mucosa.genomepool.job.fastq;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import uk.ac.ncl.mucosa.genomepool.GenomePoolDbException;
import uk.ac.ncl.mucosa.genomepool.job.ParseDataException;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.dao.FastqScoreEntryDAO;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.dao.FastqSequenceEntryDAO;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.data.FastqReport;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.data.FastqScoreEntry;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.data.FastqSequenceEntry;

/**
 * Parses a FASTQ file.
 * 
 * A FASTQ file has the following format:
 * 1) An ID line
 * 2) One or more lines of DNA sequence
 * 3) A '+' symbol
 * 4) One or more lines of quality scores
 * 
 * The next line should then be an ID line, and so on.
 * The length of the combined DNA sequences line(s) for a particular read should
 * be identical to the combined length of the quality score line(s) fro the read.
 * 
 * EG:
 * <code>
 * @SOME_ID
 * ATGCATGC...
 * ATGCATGC...
 * +
 * Score line(s)
 * Score line(s)
 * @SOME_ID (next ID)
 * ... and so on ...
 * </code>
 * 
 * The input to this parser should be a single FASTQ file. The outputs are as
 * follows:
 * <ul>
 * <li>a FastqReport and set of FastqScoreEntry and FastqSequence entries are
 * generated and stored to a MySQL database</li>.
 * <li>a DNA FASTA file is generated, containing the read IDs and associated
 * DNA sequences</li>
 * <li>a protein FASTA file is generate that contains 6-frame translations of
 * each read</li>.
 * 
 * 
 * @author Keith Flanagan
 */
public class FastqParser
{
  private static final int BATCH_SIZE = 10000;
  
  private static final String SEPARATOR = "+";
  public enum FastqState
  {
    ID_LINE, SEQUENCE, SCORES
  }
  
  /*
   * Temporary per-read variables
   */
  private FastqState state;
  private String readId;
  private StringBuilder readSeq;
  private StringBuilder readScores;
  

  /**
   * Overall database bean that represents the entire file
   */
  private FastqReport report;
  
  /**
   * A batch of sequence objects - this set is periodically stored in the
   * database as part of a batch operation
   */
  private final List<FastqSequenceEntry> parsedSeqs;
  
  /**
   * A batch of score objects - this set is periodically stored in the
   * database as part of a batch operation
   */
  private final List<FastqScoreEntry> parsedScores;
  
  public FastqParser()
  {
    this.parsedSeqs = new ArrayList<FastqSequenceEntry>();
    this.parsedScores = new ArrayList<FastqScoreEntry>();
  }
  
  /**
   * Parse a new FASTQ file. These files may be quite large, so each read is
   * parsed and stored to the database in a streaming fashion. 
   * 
   * As well as database storage, two FASTA files are (optionally) produced.
   * The first of these is a FASTA output of the reads in the FASTQ file. The
   * second contains 6-frame translations of the reads.
   * 
   * @param fastqStream an InputStream providing the FASTQ data
   * @param session a writable database connection used for storing the parsed
   * objects in a relational DB
   * @param dnaFastaStream if specified, parsed sequence data is written to this
   * stream in FASTA format
   * @param translationFastaStream if specified, 6-frame protein translations of
   * sequence data reads are written to this stream in FASTA format.
   * @param description an optional human-readable description that is attached
   * to FASTQ report entries when they are stored in a database
   * @param fasqFileURI an optional URI that is stored as meta-data alongside
   * the FASTQ entry when the database output is used. Can be used to store a
   * reference to the original FASTQ report file for provenence.
   * @return a FastqReport object that can be used to query the database.
   * @throws ParseDataException 
   */
  public FastqReport parse(InputStream fastqStream, Session session, 
      OutputStream dnaFastaStream, OutputStream translationFastaStream,
      String description, String fastqFileURI)
      throws ParseDataException
  {
    try
    {
      report = new FastqReport();
      report.setAdded(new Date(System.currentTimeMillis()));
      report.setDescription(description);
      report.setFastqFileURI(fastqFileURI);
      report.setNumReads(0);
      
      
      state = FastqState.ID_LINE;
      //Iterate each line in the file.
      BufferedReader br = new BufferedReader(new InputStreamReader(fastqStream));
      for (String line = br.readLine(); line != null; line = br.readLine())
      {
        switch(state)
        {
          case ID_LINE:
            dealIdLine(line);
            break;
          case SEQUENCE:
            dealSeqLine(line);
            break;
          case SCORES :
            dealScoreLine(line);
            dealIdLine(line);
            break;
        }
        
        //Periodically store a batch of items to the DB and files.
        if (state == FastqState.ID_LINE && parsedSeqs.size() >= BATCH_SIZE)
        {
          storeBatch(session, dnaFastaStream, translationFastaStream);
        }
      }

      //Stores final batch of items to the DB and files.
      storeBatch(session, dnaFastaStream, translationFastaStream); 
      
      return report;
    }
    catch (IOException e)
    {
      throw new ParseDataException("Failed to parse FASTQ file", e);
    }
    catch (ParseDataException e)
    {
      throw new ParseDataException("Failed to stored parsed FASTQ data", e);
    }
  }
  
  private void dealIdLine(String line)
  {
    if (state != FastqState.ID_LINE)
    {
      return;
    }
    readId = line.substring(1); //Remove first '@' character - take the rest as ID.
    readSeq = new StringBuilder();
    readScores = new StringBuilder();
    state = FastqState.SEQUENCE;
  }
  
  private void dealSeqLine(String line)
  {
    if (line.length() == 1 && line.equals(SEPARATOR))
    {
      //Reached the end of the sequence data. Change state to start reading 
      //scores
      state = FastqState.SCORES;
    }
    else
    {
      readSeq.append(line);
    }
  }
  
  private void dealScoreLine(String line)
  {
    if (readSeq.length() == readScores.length())
    {
      //We've reached the end of this entry
      FastqSequenceEntry seqEntry = new FastqSequenceEntry();
      seqEntry.setDnaSequence(readSeq.toString());
      seqEntry.setReadId(readId);
      seqEntry.setReport(report);
      parsedSeqs.add(seqEntry);
      
      FastqScoreEntry scoreEntry = new FastqScoreEntry();
      scoreEntry.setQualityScores(readScores.toString());
      scoreEntry.setReadId(readId);
      scoreEntry.setReport(report);
      parsedScores.add(scoreEntry);
      
      state = FastqState.ID_LINE; //Reset state for next entry
    }
    else
    {
      readScores.append(line);
    }
  }
  
  /**
   * Stores the content of <code>parsedSeqs</code> and 
   * <code>parsedScores</code> to a database and/or files.
   */
  private void storeBatch(Session session, 
      OutputStream dnaFastaStream, OutputStream proteinFastaStream)
      throws ParseDataException
  {
    /*
     * Write data to DB
     */
    if (session != null)
    {
      try
      {
        FastqSequenceEntryDAO seqDao = new FastqSequenceEntryDAO();
        seqDao.store(session, parsedSeqs);

        FastqScoreEntryDAO scoreDao = new FastqScoreEntryDAO();
        scoreDao.store(session, parsedScores);

        session.flush();
      }
      catch(GenomePoolDbException e)
      {
        throw new ParseDataException(
            "Failed to write FASTQ entries to a database", e);
      }
    }
    
    /*
     * Write DNA FASTA data to OutputStream
     */
    if (dnaFastaStream != null)
    {
      try
      {
        for (FastqSequenceEntry entry : parsedSeqs)
        {
          StringBuilder fastaEntry = new StringBuilder();

          fastaEntry.append(">").append(entry.getReadId()).append("\n");
          fastaEntry.append(entry.getDnaSequence()).append("\n");

          //Write FASTA-formatted DNA to OutputStream
          byte[] fastaBytes = fastaEntry.toString().getBytes();
          dnaFastaStream.write(fastaBytes);
          
          
          //Also do 6-frame translation and write to the OutputStream
          if (proteinFastaStream != null)
          {
            InputStream ramStream = new ByteArrayInputStream(fastaBytes);
            SixFrameTranslateUtil.translateSixFrames(ramStream, proteinFastaStream);
          }
        }
      }
      catch(IOException e)
      {
        throw new ParseDataException(
            "Failed to write DNA FASTA sequence(s) to stream", e);
      }
    }
    
    
    report.setNumReads(report.getNumReads()+parsedSeqs.size());
    if (parsedSeqs.size() != parsedScores.size())
    {
      throw new ParseDataException(
          "Completed parsing of a batch of reads, but number of sequences != "
          + "number of scores: "+parsedSeqs.size()+" != "+parsedScores.size());
    }
    parsedSeqs.clear();
    parsedScores.clear();
  }
  
}
