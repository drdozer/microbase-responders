/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 9, 2012, 9:07:59 PM
 */

package uk.ac.ncl.mucosa.genomepool.job.fastq;

import com.torrenttamer.hibernate.SessionProvider;
import uk.ac.ncl.mucosa.genomepool.job.gbk.*;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import org.hibernate.Session;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomeFragmentInserter;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomePoolDAO;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.dao.FastqReportDAO;
import uk.ac.ncl.mucosa.genomepool.notification.GbkEntryAvailableMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NewFastqFileNotification;
import uk.ac.ncl.mucosa.genomepool.notification.NewGbkFileMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NotificationConstants;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolConfig;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.util.file.FileUtils;

/**
 * A Microbase job implementation that takes a FASTQ-format file as 
 * input and parses the content to the Genome Pool (lite) database. 
 * In addition, two output resources are created containing the DNA and 
 * 6-frame translations of each read in FASTA format.
 * 
 * @author Keith Flanagan
 */
public class FastqParserProcessor
    extends AbstractMessageProcessor
{
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    
    // Identification of this responder
    info.setResponderName("GenomePool-fastq");
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    
    // The type of message that this responder is interested in
    info.setTopicOfInterest(NotificationConstants.IN_TOPIC_FASTQ_FOUND);
    
    // Rate limiting configuration
    info.setEnabled(true);           //Microbase will run this responder
    info.setMaxDistributedCores(30); //Max threads globally
    info.setMaxLocalCores(30);       //Max threads on this machine
    info.setPreferredCpus(1);        //Desired threads per process
    info.setQueuePopulatorRunEveryMs(5 * 1000); //When to check for new messages

    return info;
  }

  private GenomePoolConfig gpConfig;
  private SessionProvider gpLiteProvider;
  private GenomePoolDAO dao;
  
  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    try
    {
      gpConfig = new GenomePoolConfig();
      gpLiteProvider = new SessionProvider(gpConfig.getGpLiteConfigResource());
      dao = new GenomePoolDAO();
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed the preRun stage while configuring a database connection pool.", e);
    }
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    Session session = null;
    try
    {
      session = gpLiteProvider.getWriteableSession();
      session.beginTransaction();
            
      FastqReportDAO reportDao = new FastqReportDAO();

      session.getTransaction().commit();
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      e.printStackTrace();
    }
    finally
    {
      SessionProvider.silentClose(session);
    }    
    
    throw new RuntimeException("Not yet implemented!");
  }

  @Override
  public Set<Message> processMessage(Message message)
      throws ProcessingException
  {
    Session session = null;
    FileInputStream fastqStream;
    FileOutputStream dnaFastaStream = null;
    FileOutputStream translationFastaStream = null;
    try
    {
      

      throw new RuntimeException("Not yet implemented");
      
      
      
    }
    catch (Exception e)
    {
//      e.printStackTrace();
      SessionProvider.silentRollback(session);
      /*
       * Throwing a JobProcessingException here informs microbase that the job
       * execution has failed. In this case, Microbase will retry executing this
       * job again at some point in the future.
       */
      throw new ProcessingException(
          "Failed the processMessage stage while attempting to parse a "
          + "FASTQ file. Message ID: "+message.getGuid(), e);
    }
    finally
    {
      SessionProvider.silentClose(session);
//      FileUtils.closeInputStreams(fastqStream);
      FileUtils.closeOutputStreams(dnaFastaStream, translationFastaStream);
    }
  }
}
