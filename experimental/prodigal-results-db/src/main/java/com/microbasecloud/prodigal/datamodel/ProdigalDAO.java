/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author prisni
 */
public class ProdigalDAO {

    private static final int DEFAULT_BATCH = 50000;

    public void storeTranslations(Session session, List<ProdigalTranslation> proteins) throws Exception {
        try {
            int count = 0;
            for (ProdigalTranslation pd : proteins) {
                session.save(pd);
                count++;
                if (count % DEFAULT_BATCH == 0) {
                    session.flush();
                    session.clear();
                }

            }


        } catch (Exception e) {
            throw new Exception("Failed to store", e);
        }
    }

    public void storeCdss(Session session, List<ProdigalCDS> cdss) throws Exception {
        try {
            int count = 0;
            for (ProdigalCDS cd : cdss) {
                session.save(cd);
                count++;
                if (count % DEFAULT_BATCH == 0) {
                    session.flush();
                    session.clear();
                }

            }
        } catch (Exception e) {
            throw new Exception("Failed to store", e);
        }
    }
}
