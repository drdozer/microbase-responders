/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 30-Sep-2012, 12:55:35
 */

package com.microbasecloud.blast;

import com.microbasecloud.blast.common.BlastJobDescription;
import com.microbasecloud.blast.common.BlastJobDescription.BlastType;
import com.microbasecloud.blast.common.Constants;
import java.io.IOException;
import java.util.Properties;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;

/**
 * This program generates a Microbase notification message containing
 * a BlastJobDescription, given a suitable database and query file.
 * 
 * This program is provided for convenience and produces both the text for a
 * message, and a sample notification system command line that can be used to
 * initiate the blast responder.
 * 
 * @author Keith Flanagan
 */
public class JobMessageGenerator
{
  public static void main(String[] args) throws Exception
  {
    if (args.length != 8) {
      System.out.println("USAGE:\n"
          + "  * <bucket> <path> <filename> - a BLAST database\n"
          + "  * <database name> - the name of the DB (excluding file extensions)"
          + "  * <bucket> <path> <filename> - a FASTA query file\n"
          + "  * <blastn|blastp> - the type of BLAST to run");
      System.exit(1);
    }
    
    // Create message content
    BlastJobDescription job = new BlastJobDescription();
    int idx = 0;
    job.setSubjectDbBucket(args[idx++]);
    job.setSubjectDbPath(args[idx++]);
    job.setSubjectDbFilename(args[idx++]);
    
    job.setSubjectDatabaseName(args[idx++]);
    
    job.setQueryBucket(args[idx++]);
    job.setQueryPath(args[idx++]);
    job.setQueryFilename(args[idx++]);
    
    job.setJobType(BlastType.valueOf(args[idx++].toLowerCase()));
    
    job.setResultBucket(job.getQueryBucket());
    job.setResultPath(job.getQueryPath());
    job.setResultFilename(job.getQueryFilename()+"-vs-"+job.getSubjectDatabaseName()+"."+job.getJobType().name());
    
    
    
    
    // Serialise message content to JSON
    ObjectMapper mapper = new ObjectMapper();
    String jobJson = mapper.writeValueAsString(job);
    
    System.out.println("Message content:");
    System.out.println(jobJson);
    
    // Load responder configuration file to determine topic name
    Properties config = new Properties();
    config.load(BlastExeResponder.class.getResourceAsStream(BlastExeResponder.CONFIG_RESOURCE));

    ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);
    
    
    // Create notification.sh command line
    /*
     * notification.sh --publish -t GENE-PREDICTION-JOB -p manual-publisher 
     * -r prodigal-resultset -c seq_bucket="prisni-prodigal-fasta-sequences" 
     * -c seq_path="fasta-sequences/test-sequences" -c seq_filename="s_aureus_contig_24.fasta"
     */
    StringBuilder cmd = new StringBuilder();
    cmd.append("notification.sh --publish");
    cmd.append(" -t ").append(info.getTopicOfInterest());
    cmd.append(" -r test-results");
    cmd.append(" -c ").append(Constants.MSG_PROP__JOB).append("=\"")
        .append(jobJson).append("\"");
    
    System.out.println("Notification command:");
    System.out.println(cmd.toString());
    
  }
}
