/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.blast;

import com.microbasecloud.blast.common.BlastJobDescription;
import com.microbasecloud.blast.common.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;
import uk.org.microbase.util.file.FileUtils;
import uk.org.microbase.util.file.zip.ZipUtils;

/**
 * A MessageProcessor implementation capable of executing tools from
 * the NCBI BLAST package.
 * 
 * Input message requirements:
 *   - dbase.bucket
 *   - dbase.path
 *   - dbase.filename
 *   - query.bucket
 *   - query.path
 *   - query.filename
 * 
 *   - result.bucket
 *   - result.path
 *   - result.filename
 * 
 * Input message optional:
 *   - m {format}
 * 
 * 
 * 
 * @author Keith Flanagan
 */
public class BlastExeResponder
  extends AbstractMessageProcessor
{
  private static final Logger l = Logger.getLogger(BlastExeResponder.class.getName());
  
  static final String CONFIG_RESOURCE = "/"+BlastExeResponder.class.getSimpleName()+".properties";

  private Properties config;
    
  @Override
  protected ResponderInfo createDefaultResponderInfo()
      throws RegistrationException
  {
    try
    {
      config = new Properties();
      config.load(BlastExeResponder.class.getResourceAsStream(CONFIG_RESOURCE));

      ResponderInfo info = 
          ResponderInfoPropertiesParser.parseFromProperties(config);
      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  }
  
  private BlastJobDescription parseJobDescrFrom(Message msg)
      throws IOException
  {
    String jobDescrRawJson = msg.getContent().get(Constants.MSG_PROP__JOB);
    
    ObjectMapper mapper = new ObjectMapper();
//    JsonNode jobDescrJson = mapper.readValue(jobDescrRawJson, JsonNode.class);
//    int val = jobDescrJson.get("return").get("rights").get("info").asInt();
    
    BlastJobDescription jobDescr = mapper.readValue(jobDescrRawJson, BlastJobDescription.class);
    return jobDescr;
  }
  

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    l.info("Doing preRun");

  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    l.info("Doing cleanup");
    try
    {
      BlastJobDescription job = parseJobDescrFrom(message);
      MicrobaseFS fs = getRuntime().getMicrobaseFS();
      if (fs.exists(
          job.getResultBucket(), job.getResultPath(), job.getResultFilename()))
      {
        l.info("Deleting old result file");
        fs.deleteRemoteCopy(job.getResultBucket(), job.getResultPath(), job.getResultFilename());
      }
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
  }

  @Override
  public Set<Message> processMessage(Message inMsg)
      throws ProcessingException
  {
   l.info("Performing computational work");
    try
    {
      BlastJobDescription job = parseJobDescrFrom(inMsg);
      
      FileMetaData seqFileDescr = new FileMetaData(
          job.getQueryBucket(), job.getQueryPath(), job.getQueryFilename());
      FileMetaData dbZipFileDescr = new FileMetaData(
          job.getSubjectDbBucket(), job.getSubjectDbPath(), job.getSubjectDbFilename());
      
      // Download the query seqeunce to the scratch space for this job.
      File querySeqFile = downloadInputFile(seqFileDescr, getWorkingDirectory());
      
      /*
       * The database is potentially huge, so download and extract it to the
       * shared scratch space for two reasons:
       * 1) other responders/instances may require the same DB concurrently
       * 2) the shared scratch space persists beyond individual job executions
       */
      File dbDir = downloadAndUnzipFileToSharedSpace(dbZipFileDescr);
      
      l.info("Running command ...");
      File alignmentFile = runCommand(querySeqFile, dbDir, 
          job.getSubjectDatabaseName(), job.getJobType());
      
      l.info("Uploading completed alignment: "+alignmentFile.getAbsolutePath());
      String resultBucket = getResponderInfo().getDataDestinationBucketOverride();
      String resultPath = getResponderInfo().getResponderId();
      getRuntime().getMicrobaseFS().upload(
          alignmentFile, 
          resultBucket, resultPath, alignmentFile.getName(), null);
      
      
      
      Message successMsg = generateSuccessNotification(inMsg, job);
      
      Set<Message> messages = new HashSet<Message>();
      messages.add(successMsg);
      return messages;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run one or more commands: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
  }
  
  
  private File downloadInputFile(FileMetaData remoteFile, File destDir)
      throws ProcessingException
  {
    try
    { 
      //Download file
      File destinationFile = new File(destDir, remoteFile.getName());
      
      getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
          remoteFile.getBucket(), remoteFile.getPath(), remoteFile.getName(), 
          destinationFile, true);
      
      return destinationFile;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to download remote file: " 
          + remoteFile.getBucket()+", "
          + remoteFile.getPath() + ", "
          + remoteFile.getName(), e);
    }
  }
  
  
  private File runCommand(File seqFile, File dbDir, String dbName,
      BlastJobDescription.BlastType blastType)
      throws ProcessingException
  {
    String cmdLineText = null;
    try
    {
      File alignmentOutput = new File(getWorkingDirectory(), seqFile.getName()+"-vs-"+dbName+"."+blastType.name());
      String[] command = new String[]
      {
        "blastall",
        "-p", blastType.name(),
        //"-e", String.valueOf(eValue),
        "-m", "8",
        "-a", String.valueOf(getProcessInfo().getAssignedLocalCores()),
        "-i", seqFile.getAbsolutePath(),
        "-d", new File(dbDir, dbName).getAbsolutePath(),
        //"-j", subjectSeqFasta.getAbsolutePath(), //Second sequence
        "-o", alignmentOutput.getAbsolutePath()
      };
      cmdLineText = Arrays.asList(command).toString();

      executeCommand(command, blastType.name());
   
      return alignmentOutput;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to execute the command line: "+cmdLineText, e);
    }
  }

  
  private void executeCommand(String[] command, String commandType)
      throws ProcessingException
  {
    File workDir = getWorkingDirectory();
    StringBuilder namePrefix = new StringBuilder();
    namePrefix.append(getProcessInfo().getMessageId()).append(".");
    namePrefix.append(getProcessInfo().getProcessGuid()).append(".");
    namePrefix.append(getProcessInfo().getHostname()).append(".");
    namePrefix.append(commandType).append(".");
    File stdOutFile = new File(workDir, namePrefix.toString()+"stdout.txt");
    File stdErrFile = new File(workDir, namePrefix.toString()+"stderr.txt");
    try {
      FileWriter stdOut = new FileWriter(stdOutFile);
      FileWriter stdErr = new FileWriter(stdErrFile);

      int exitStatus = NativeCommandExecutor.executeNativeCommand(
          workDir, command, stdOut, stdErr);

      stdOut.flush();
      stdOut.close();
      stdErr.flush();
      stdErr.close();

      if (exitStatus != 0) {
        throw new ProcessingException("Exit status of the command: "
            + Arrays.asList(command)
            + " \nwas "+exitStatus
            + ". Assuming that non-zero means that execution failed.");
      }
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to execute command: "+command[0], e);
    }
    finally {
      try {
        String logBucket = getResponderInfo().getLogDestinationBucketOverride();
        String logPath = getResponderInfo().getResponderId();
        
        getRuntime().getMicrobaseFS().upload(
            stdOutFile, logBucket, logPath, stdOutFile.getName(), null);
        getRuntime().getMicrobaseFS().upload(
            stdErrFile, logBucket, logPath, stdErrFile.getName(), null);
      }
      catch(Exception e) {
        throw new ProcessingException(
            "Failed to upload STDOUT/STDERR content", e);
      }
    }
  }
  

  private Message generateSuccessNotification(Message parent, BlastJobDescription job)
      throws IOException
  {
    Message successMsg = createMessage(
        parent,                           //The message to use as a parent
        getResponderInfo().getTopicOut(), //The topic of the new message
        parent.getWorkflowStepId(),        //The workflow step ID
        //An optional human-readable description
        "Successfully completed a BLAST analysis.");
    
    
    ObjectMapper mapper = new ObjectMapper();
    String jobJson = mapper.writeValueAsString(job);
    successMsg.getContent().put(Constants.MSG_PROP__JOB, jobJson);

    
    return successMsg;
  }
  
  
}
