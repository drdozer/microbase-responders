/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 28-Sep-2012, 23:20:47
 */

package com.microbasecloud.blast.common;

import java.io.Serializable;

/**
 *
 * @author Keith Flanagan
 */
public class BlastJobDescription
    implements Serializable
{
  public enum BlastType {
    blastn, blastp;
  };
  
  private BlastType jobType;
  
  private String subjectDbBucket;
  private String subjectDbPath;
  private String subjectDbFilename;
  
  private String subjectDatabaseName;
  
  private String queryBucket;
  private String queryPath;
  private String queryFilename;
  
  private String resultBucket;
  private String resultPath;
  private String resultFilename;
  
  public BlastJobDescription()
  {
  }

  public String getSubjectDbBucket()
  {
    return subjectDbBucket;
  }

  public void setSubjectDbBucket(String subjectDbBucket)
  {
    this.subjectDbBucket = subjectDbBucket;
  }

  public String getSubjectDbPath()
  {
    return subjectDbPath;
  }

  public void setSubjectDbPath(String subjectDbPath)
  {
    this.subjectDbPath = subjectDbPath;
  }

  public String getSubjectDbFilename()
  {
    return subjectDbFilename;
  }

  public void setSubjectDbFilename(String subjectDbFilename)
  {
    this.subjectDbFilename = subjectDbFilename;
  }

  public String getQueryBucket()
  {
    return queryBucket;
  }

  public void setQueryBucket(String queryBucket)
  {
    this.queryBucket = queryBucket;
  }

  public String getQueryPath()
  {
    return queryPath;
  }

  public void setQueryPath(String queryPath)
  {
    this.queryPath = queryPath;
  }

  public String getQueryFilename()
  {
    return queryFilename;
  }

  public void setQueryFilename(String queryFilename)
  {
    this.queryFilename = queryFilename;
  }

  public String getResultBucket()
  {
    return resultBucket;
  }

  public void setResultBucket(String resultBucket)
  {
    this.resultBucket = resultBucket;
  }

  public String getResultPath()
  {
    return resultPath;
  }

  public void setResultPath(String resultPath)
  {
    this.resultPath = resultPath;
  }

  public String getResultFilename()
  {
    return resultFilename;
  }

  public void setResultFilename(String resultFilename)
  {
    this.resultFilename = resultFilename;
  }

  public String getSubjectDatabaseName()
  {
    return subjectDatabaseName;
  }

  public void setSubjectDatabaseName(String subjectDatabaseName)
  {
    this.subjectDatabaseName = subjectDatabaseName;
  }

  public BlastType getJobType()
  {
    return jobType;
  }

  public void setJobType(BlastType jobType)
  {
    this.jobType = jobType;
  }
}
