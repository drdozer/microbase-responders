/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.mira.responder.assemblermiraresponder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;
import uk.org.microbase.util.file.zip.ZipUtils;

    
/**
 *
 * @author prisni
 */
public class SFFResponder extends AbstractMessageProcessor{
    
    private static final String TOPIC_IN = "FASTQ-GENERATION-JOB";
    private static final String TOPIC_OUT = "ASSEMBLY-JOB-BEGIN";
    
    
    private FileMetaData sff;
    private String projectName = "Strain-4";
    private FileMetaData fastQS3File;
    private FileMetaData traceXMLS3File;
    private String logFileS3Bucket;
    private String logFileS3Path;
    

    @Override
    protected ResponderInfo createDefaultResponderInfo() {
        ResponderInfo info = new ResponderInfo();
        info.setEnabled(true);
        info.setMaxDistributedCores(300);
        info.setMaxLocalCores(50);
        info.setPreferredCpus(1);
        info.setQueuePopulatorLastRunTimestamp(0);
        info.setQueuePopulatorRunEveryMs(30000);
        info.setResponderName(SFFResponder.class.getSimpleName());
        info.setResponderVersion("1.0");
        info.setResponderId(info.getResponderName() + "-"
                + info.getResponderVersion());
        info.setTopicOfInterest(TOPIC_IN);
        return info;
    }

    @Override
    public void preRun(Message msg) throws ProcessingException {
       System.out.println("Doing pre-run");
        sff = parseZipFileFromMessage(msg);
        // set remote location for output
        String remoteS3Bucket = sff.getBucket();
        String remoteS3Path = "fastQ from: " + sff.getName();
        String remoteFastQS3Name = projectName + "_in.iontor" +
                ".fastq";
        fastQS3File = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteFastQS3Name);
         String remoteTraceXMLS3Name = projectName + "_traceinfo_in.iontor" +
                ".xml";
        traceXMLS3File = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteTraceXMLS3Name);
        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;
    }

    @Override
    public void cleanupPreviousResults(Message msg) throws ProcessingException {
        
    }

    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        try {

            File sffFileZip = downloadInputFile(sff);
            InputStream zippedFIS = new FileInputStream(sffFileZip);
            ZipUtils.extractZip(zippedFIS, getWorkingDirectory());
            zippedFIS.close();
            
            // MIRA responder takes an sff file which is zipped. We extract the sff file(s)
            // We're assuming that this zipped file has only one sff file 
            // and has the same name as the zipped file
            String sff_filename = parseSffFileNameFromMessage(msg);
            
            File sffFile = new File(getWorkingDirectory(), sff_filename);
            
            File fastQResultFile = new File(getWorkingDirectory(),
                    fastQS3File.getName());
            File traceXMLResultFile = new File(getWorkingDirectory(),
                    traceXMLS3File.getName());
            String[] sffExtractCommand = new String[]{"/mnt/responder_data/sff_extract_0_3_0", "-s",
                fastQResultFile.getAbsolutePath(), "-x",
                traceXMLResultFile.getAbsolutePath(),
                sffFile.getAbsolutePath()};
            executeCommand(sffExtractCommand, "sff_extract");
            Message sffSuccessMsg = generateSuccessNotification(msg);
            Set<Message> messages = new HashSet<Message>();
            messages.add(sffSuccessMsg);
            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);

        }
    }

    private File downloadInputFile(FileMetaData remoteFile) 
            throws ProcessingException {
         try {
            //get a reference to the working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the fasta file
            File destinationFile = new File(tmpDir, remoteFile.getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
                    remoteFile.getBucket(), remoteFile.getPath(),
                    remoteFile.getName(), destinationFile, true);

            return destinationFile;

        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file"
                    + remoteFile.getBucket() + " , " + remoteFile.getPath(), e);
        }
    }
    /**
     * File descriptor for zip file in S3 bucket
     * @param m
     * @return
     * @throws ProcessingException 
     */

    private FileMetaData parseZipFileFromMessage(Message m) throws ProcessingException{
        try {
            FileMetaData sffZipFile = new FileMetaData(m.getContent()
                    .get("sffZip_bucket"),
                    m.getContent().get("sffZip_path"),
                    m.getContent().get("sffZip_filename"));
            return sffZipFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
    }
    
    /**
     * name of the file within that zipped file
     * @param m
     * @return
     * @throws ProcessingException 
     */
    
    private String parseSffFileNameFromMessage(Message m) throws ProcessingException{
        try {
            return m.getContent().get("sff_filename");
        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
    }

    
    private Message generateSuccessNotification(Message parent) {
       
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully accomplished extracting fastq and trace info xml using ssf_extract for the sff file: "
                    + sff.getName());
//            successMsg.getContent().put("sff_bucket", sff.getBucket());
//            successMsg.getContent().put("sff_path", sff.getPath());
//            successMsg.getContent().put("sff_filename", sff.getName());
            
            // Inform future responders about the fastq output file
            successMsg.getContent().put("result_file.fastQ.bucket", fastQS3File.getBucket());
            successMsg.getContent().put("result_file.fastQ.path", fastQS3File.getPath());
            successMsg.getContent().put("result_file.fastQ.filename", fastQS3File.getName());
            
            // Inform future responders about the trace info xml output file
            successMsg.getContent().put("result_file.traceXML.bucket", traceXMLS3File.getBucket());
            successMsg.getContent().put("result_file.traceXML.path", traceXMLS3File.getPath());
            successMsg.getContent().put("result_file.traceXML.filename", traceXMLS3File.getName());
            return successMsg;
        }
    
    
    private void executeCommand(String[] command, String stdOutPrefix) 
            throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir,
                    command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command) + "\nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: "
                    + command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, logFileS3Bucket,
                        logFileS3Path, stdOutFile.getName(), null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, logFileS3Bucket,
                        logFileS3Path, stdErrFile.getName(), null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }


  
    }    
