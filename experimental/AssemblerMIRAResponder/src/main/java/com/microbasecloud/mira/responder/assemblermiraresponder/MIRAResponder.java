/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.mira.responder.assemblermiraresponder;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class MIRAResponder extends AbstractMessageProcessor {
    
    private static final String TOPIC_IN = "ASSEMBLY-JOB-BEGIN";
    private static final String TOPIC_OUT = "ASSEMBLY-JOB-ACCOMPLISHED";
    private FileMetaData readsFastQ; //projectName_in.iontor.fastq file stored in S3 bucket
    private FileMetaData readsTraceInfo; // projectName_traceinfo_in.iontor.xml file stored in S3
    private String logFileS3Bucket; // bucket where these files are stored
    private String logFileS3Path; // path of the files
    private FileMetaData miraAssemblyLogS3Directory; //file meta-data for the output files
    IOParameters input = new IOParameters();
    String projectName = "Strain-4"; // set project name as "Strain-4"
    
    /**
     * Create a default responder with pre-set configuration properties
     * @return responder information
     */
    @Override
    protected ResponderInfo createDefaultResponderInfo() {
        ResponderInfo info = new ResponderInfo();
        info.setEnabled(true);
        info.setMaxDistributedCores(300);
        info.setMaxLocalCores(50);
        info.setPreferredCpus(1);
        info.setQueuePopulatorLastRunTimestamp(0);
        info.setQueuePopulatorRunEveryMs(30000);
        info.setResponderName(MIRAResponder.class.getSimpleName());
        info.setResponderVersion("1.0");
        info.setResponderId(info.getResponderName() + "-"
                + info.getResponderVersion());
        info.setTopicOfInterest(TOPIC_IN);
        return info;
    }

    public void preRun(Message msg) throws ProcessingException {
        System.out.println("Doing pre-run");
        readsFastQ = parseFastQfromMessage(msg);
        readsTraceInfo = parseTraceInfofromMessage(msg);
        // set remote location for output
        String remoteS3Bucket = readsFastQ.getBucket(); //sets output bucket to be the same as fastq
        String remoteS3Path = "assembly for: " + projectName; //sets the path for the output to be uploaded to
        String remoteAssemblyLogS3Name = "my" + "-" + projectName + "-" +
                "assembly"; //sets the name for the output directory
        miraAssemblyLogS3Directory = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteAssemblyLogS3Name); // creats a filemetadata object for the mira assembly results
        /**
         * might need another meta data to define directory upload
         */
        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;
    }
    //note: miraAssemblyLogS3Name is the name of the output directory in the S3 bucket
    // where the assembly results will be stored

    public void cleanupPreviousResults(Message msg)
            throws ProcessingException {
        
    }

    public Set<Message> processMessage(Message msg) 
            throws ProcessingException {
        try {
            input.setAssemblyType("denovo");
            input.setSequenceType("genome");
            input.setDataValueType("accurate");
            input.setSequencingPlatform("iontor");
            
            
            File assemblyResults = miraAssembler(projectName); //runs MIRA
            
            
            uploadResults(assemblyResults); //uploads the resulting directory to the S3 bucket
            
            
            
            Message assemblyMsg = generateSuccessNotification(msg); //generate success notification
            Set<Message> messages = new HashSet<Message>(); //create a hash set of notification messages 
            messages.add(assemblyMsg); //add new success messages to the hash set
            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);

        }
       
    }
/**
 * Parses the fastq file from the S3 bucket
 * @param m
 * @return
 * @throws ProcessingException 
 */
    private FileMetaData parseFastQfromMessage(Message m) 
            throws ProcessingException {
             try {
            FileMetaData readsFastQ = new FileMetaData(m.getContent().
                    get("FastQ_bucket"),
                    m.getContent().get("FastQ_path"),
                    m.getContent().get("FastQ_filename"));
            return readsFastQ;
        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
    }
    /**
     * parses the trace info xml file from the S3 bucket
     * @param m
     * @return
     * @throws ProcessingException 
     */
     private FileMetaData parseTraceInfofromMessage(Message m) 
            throws ProcessingException {
             try {
            FileMetaData readsTraceInfo = new FileMetaData(m.getContent().
                    get("TraceInfo_bucket"),
                    m.getContent().get("TraceInfo_path"),
                    m.getContent().get("TraceInfo_filename"));
            return readsTraceInfo;
        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
     }
     /**
      * function to download input file to the working directory
      * @param remoteFile
      * @return
      * @throws ProcessingException 
      */
     private File downloadInputFile(FileMetaData remoteFile) throws
            ProcessingException {
        try {
            //get a reference to the working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the fastq file
            File destinationFile = new File(tmpDir, remoteFile.getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
                    remoteFile.getBucket(), remoteFile.getPath(),
                    remoteFile.getName(), destinationFile, true);

            return destinationFile;

        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file"
                    + remoteFile.getBucket() + " , " + remoteFile.getPath());
        }
    }
    private Message generateSuccessNotification(Message parent) {
        {
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully accomplished assembly job using MIRA for the reads: "
                    + input.getProjectName());
//            successMsg.getContent().put("seq_bucket", seq.getBucket());
//            successMsg.getContent().put("seq_path", seq.getPath());
//            successMsg.getContent().put("seq_filename", seq.getName());

//             Inform future responders about the output assembly files
            successMsg.getContent().put("result_file.assembled_reads_report.bucket", miraAssemblyLogS3Directory.getBucket());
            successMsg.getContent().put("result_file.assembled_reads_report.path", miraAssemblyLogS3Directory.getPath());
            successMsg.getContent().put("result_file.assembled_reads_report.dirname", miraAssemblyLogS3Directory.getName());


            return successMsg;
        }

    }
     
     private void executeCommand(String[] command, String stdOutPrefix) throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command) + "\nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: "
                    + command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, logFileS3Bucket,
                        logFileS3Path, stdOutFile.getName(), null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, logFileS3Bucket,
                        logFileS3Path, stdErrFile.getName(), null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }
/**
 * NEEDS ATTENTION (has to be a directory structure as a return
 * @param projectName
 * @return
 * @throws ProcessingException 
 */
    private File miraAssembler(String projectName) throws ProcessingException {
        try {
            getWorkingDirectory();
            downloadInputFile(readsFastQ);
            downloadInputFile(readsTraceInfo);
                        File assemblyResultLogFile = new File(getWorkingDirectory(),
                    miraAssemblyLogS3Directory.getName());
            String[] miraCommand;
            miraCommand = new String[]{"/mnt/responder_data/mira", "-GE:not=8", "--project=",
                projectName, "job=", input.getAssemblyType(), input.getSequenceType(),
                input.getDataValueType(), input.getSequencingPlatform(), ">",
                assemblyResultLogFile.getAbsolutePath()};
                executeCommand(miraCommand, "mira");

            return assemblyResultLogFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute MIRA assembler", e);
        }
    }
   private void uploadResults(File assemblyResult) throws FSOperationNotSupportedException, FSException{
       
       
         getRuntime().getMicrobaseFS().upload(
                    assemblyResult,
                    miraAssemblyLogS3Directory.getBucket(), miraAssemblyLogS3Directory.getPath(),
                    miraAssemblyLogS3Directory.getName(), null);
    }
}
   
    
    
