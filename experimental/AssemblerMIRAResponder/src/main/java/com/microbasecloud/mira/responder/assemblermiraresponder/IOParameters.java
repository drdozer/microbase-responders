/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.mira.responder.assemblermiraresponder;

import java.io.File;

/**
 *
 * @author prisni
 */
public class IOParameters {

    private String ProjectName = new String();
    private String SequencingPlatform = new String();
    private String AssemblyType = new String();
    private String SequenceType = new String();
    private String DataValueType = new String();

    // Constructor
    public IOParameters() {
    }

    public String getSequencingPlatform() {
        return SequencingPlatform;
    }

    public void setSequencingPlatform(String SequencingPlatform) {
        this.SequencingPlatform = SequencingPlatform;
    }

    public String getAssemblyType() {
        return AssemblyType;
    }

    public void setAssemblyType(String AssemblyType) {
        this.AssemblyType = AssemblyType;
    }

    public String getSequenceType() {
        return SequenceType;
    }

    public void setSequenceType(String SequenceType) {
        this.SequenceType = SequenceType;
    }

    public String getDataValueType() {
        return DataValueType;
    }

    public void setDataValueType(String DataValueType) {
        this.DataValueType = DataValueType;
    }

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String ProjectName) {
        this.ProjectName = ProjectName;
    }
}
