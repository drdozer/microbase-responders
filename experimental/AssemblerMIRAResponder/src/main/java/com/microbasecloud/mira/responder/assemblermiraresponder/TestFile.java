/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.mira.responder.assemblermiraresponder;

import java.io.File;

/**
 *
 * @author prisni
 */
public class TestFile {
    
    public static void main(String[] args){
        File[] files = new File("/home/prisni/Documents").listFiles();
        showFiles(files);
        
    }

    private static void showFiles(File[] files) {
        for (File file:files) {
            if(file.isDirectory()) {
                System.out.println("\ndirectory: "+file.getName());
                showFiles(file.listFiles());
            }
            else {
                System.out.println("file: "+file.getName());
            }
        }
    }
    
}
