/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.responders.filescanner;

import com.microbasecloud.filescanner.common.Constants;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;

/**
 * 
 * @author Keith Flanagan
 */
public class FileScannerResponder
  extends AbstractMessageProcessor
{
  private static final Logger l = Logger.getLogger(FileScannerResponder.class.getName());
  
  static final String CONFIG_RESOURCE = "/"+FileScannerResponder.class.getSimpleName()+".properties";

  private final ScheduledThreadPoolExecutor exe;
  
  public FileScannerResponder()
  {
    exe = new ScheduledThreadPoolExecutor(1);

  }

  @Override
  public void initialise()
      throws RegistrationException
  {
    l.info("Initialising responder");
    super.initialise();
    
    // Lock ensures that only one filescanner can run at any one time
    l.info("Acquring cluster-wide lock - only one instance can run at a time");
    Lock lock = getRuntime().getHazelcast().getLock(Constants.HZ_LAST_RUN_MAP);
    lock.lock();
    try
    {
      Properties config = new Properties();
      config.load(FileScannerResponder.class.getResourceAsStream(CONFIG_RESOURCE));
    
      Set<Target> targets = ReadConfig.loadConfig(config);
      Map<String, Long> targetLastScannedAt = 
          getRuntime().getHazelcast().getMap(Constants.HZ_LAST_RUN_MAP);
      l.info("There are "+targets.size()+ " to scan. Last scan times: "+targetLastScannedAt);
      
      // Create a worker thread for targets if they need to be scanned
      long now = System.currentTimeMillis();
      for (Target target : targets)
      {
        String bucketPath = target.getBucket()+";"+target.getPath();
        Long lastScanned = targetLastScannedAt.get(bucketPath);
        if (lastScanned == null || lastScanned + target.getScanEveryMs() < now)
        {
          l.info("Scheduling worker for scan target: "+target.getBucket()+"; "+target.getPath());
          MbfsScannerWorker worker = new MbfsScannerWorker(getRuntime(), target);
          exe.schedule(worker, 0, TimeUnit.MILLISECONDS);
          targetLastScannedAt.put(bucketPath, now);
        }
        else 
        {
          l.info("NOT scheduling worker for scan target: "
              +target.getBucket()+"; "+target.getPath()
              + " because the target was scanned recently.");
        }
      }
      
      
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
    finally
    {
      l.info("Releasing cluster-wide lock.");
      lock.unlock();
    }
    
    /*
     * When this method ends, this instance of the FileScanner responder may
     * well be dereferenced by the Microbase client since the client will
     * soon determine that no messages need to be processd. One concern may be
     * that the ScheduledThreadPoolExecutor also becomes dereferenced.
     * In fact, this doesn't matter, since it turns out that the executor will
     * only be finalized when all threads have finished executing:
     * http://docs.oracle.com/javase/6/docs/api/java/util/concurrent/ThreadPoolExecutor.html
     */
  }

  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
      throws RegistrationException
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(1);
    info.setMaxLocalCores(1);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(FileScannerResponder.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest("N/A");
    return info;
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    l.info("Doing preRun");

  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    l.info("Doing cleanup");
  }

  @Override
  public Set<Message> processMessage(Message inMsg)
      throws ProcessingException
  {
   l.info("Performing computational work");
   return new HashSet<>();
  }
  
  
  
}
