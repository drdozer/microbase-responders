/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-Oct-2012, 22:53:33
 */

package com.microbasecloud.responders.filescanner;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Keith Flanagan
 */
public class Target
{
  private String targetName;
  
  private String bucket;
  private String path;
  private Set<String> extensions;
  private long scanEveryMs;
  private String newFileTopic;
  private String updatedFileTopic;
  private String deletedFileTopic;
  private String unchangedFileTopic;
  private String digestTopic;

  public Target()
  {
    extensions = new HashSet<>();
  }

  public String getBucket()
  {
    return bucket;
  }

  public void setBucket(String bucket)
  {
    this.bucket = bucket;
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }

  public Set<String> getExtensions()
  {
    return extensions;
  }

  public void setExtensions(Set<String> extensions)
  {
    this.extensions = extensions;
  }

  public long getScanEveryMs()
  {
    return scanEveryMs;
  }

  public void setScanEveryMs(long scanEveryMs)
  {
    this.scanEveryMs = scanEveryMs;
  }

  public String getNewFileTopic()
  {
    return newFileTopic;
  }

  public void setNewFileTopic(String newFileTopic)
  {
    this.newFileTopic = newFileTopic;
  }

  public String getUpdatedFileTopic()
  {
    return updatedFileTopic;
  }

  public void setUpdatedFileTopic(String updatedFileTopic)
  {
    this.updatedFileTopic = updatedFileTopic;
  }

  public String getDeletedFileTopic()
  {
    return deletedFileTopic;
  }

  public void setDeletedFileTopic(String deletedFileTopic)
  {
    this.deletedFileTopic = deletedFileTopic;
  }

  public String getUnchangedFileTopic()
  {
    return unchangedFileTopic;
  }

  public void setUnchangedFileTopic(String unchangedFileTopic)
  {
    this.unchangedFileTopic = unchangedFileTopic;
  }

  public String getDigestTopic()
  {
    return digestTopic;
  }

  public void setDigestTopic(String digestTopic)
  {
    this.digestTopic = digestTopic;
  }

  public String getTargetName()
  {
    return targetName;
  }

  public void setTargetName(String targetName)
  {
    this.targetName = targetName;
  }
}
