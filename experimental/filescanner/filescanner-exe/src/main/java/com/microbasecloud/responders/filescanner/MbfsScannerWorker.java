/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 06-Oct-2012, 21:44:44
 */

package com.microbasecloud.responders.filescanner;

import com.microbasecloud.filescanner.common.Constants;
import com.microbasecloud.filescanner.common.FileStatus;
import com.microbasecloud.filescanner.notification.FileStateChanged;
import com.microbasecloud.responders.filescanner.data.FileEntry;
import com.microbasecloud.responders.filescanner.data.FileEntryDAO;
import com.microbasecloud.responders.filescanner.data.ScanReport;
import com.microbasecloud.responders.filescanner.data.ScanReportDAO;
import com.torrenttamer.hibernate.SessionProvider;
import com.torrenttamer.util.UidGenerator;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import uk.org.microbase.filesystem.spi.FileInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.notification.spi.NotificationException;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.serialization.JsonSerializer;
import uk.org.microbase.util.serialization.MbSerializerException;

/**
 *
 * @author Keith Flanagan
 */
public class MbfsScannerWorker
    implements Runnable
{
  private static final Logger logger =
      Logger.getLogger(MbfsScannerWorker.class.getName());
  
  private static final String PUBLISHER = FileScannerResponder.class.getSimpleName();
  
  private final ClientRuntime runtime;
  private final Target target;
  
  
  public MbfsScannerWorker(ClientRuntime runtime, Target target)
  {
    this.runtime = runtime;
    this.target = target;
  }
  
  /**
   * Utility method to compare two dates down to an accuracy of 1 second.
   * This is needed because whilst Java Dates typically measure milliseconds,
   * the SQL temporal type 'timestamp' uses second-level accuracy.
   * This method returns true if the dates are equal to the nearset second.
   * 
   * @param first
   * @param second
   * @return 
   */
  private boolean datesEqual(Date first, Date second)
  {
    logger.info("First timestamp: "+first.getTime()
        +", second timestamp: "+second.getTime());
    GregorianCalendar firstCal = new GregorianCalendar();
    firstCal.setTime(first);
    
    GregorianCalendar secondCal = new GregorianCalendar();
    secondCal.setTime(second);
    
    firstCal.set(Calendar.MILLISECOND, 0);
    secondCal.set(Calendar.MILLISECOND, 0);
    
//    logger.info("First: "+firstCal.toString()+", second: "+secondCal.toString());
    return firstCal.equals(secondCal);
  }
   
  @Override
  public void run()
  {
    Session session = null;
    
    try
    {
      logger.log(Level.INFO,
          "Performing file scan of remote filesystem: {0}: {1}", 
          new Object[]{target.getBucket(), target.getPath()});
      
      // Perform scan of requested target - ignore files not of interest.
      Set<FileMetaData> files = runtime.getMicrobaseFS()
          .listRemoteFileMetaData(target.getBucket(), target.getPath());
      for (Iterator<FileMetaData> itr = files.iterator(); itr.hasNext();) {
        FileMetaData file = itr.next();
//        logger.log(Level.INFO, "Found remote file: {0}; {1}; {2}", 
//            new Object[]{file.getBucket(), file.getPath(), file.getName()});
        boolean matched = false;
        for (String ext : target.getExtensions()) {
          if (file.getName().endsWith(ext)) {
            matched = true;
//            logger.info("Remote matched extension: "+ext);
            break;
          }
        }
        if (!matched) {
          itr.remove();
        }
      }
     
      
      logger.info("Obtaining connection to database");
      ScanReportDAO reportDao = new ScanReportDAO();
      FileEntryDAO fileDao = new FileEntryDAO();
      SessionProvider sp = new SessionProvider(Constants.HIBERNATE_RESOURCE);
      
      
//      session = sp.getReadOnlySession();
      session = sp.getWriteableSession();
      session.beginTransaction();
      
      // Obtain the previous scan report for this target (is there is one)
      logger.info("Obtaining previous scan report for this target (if present)");
      ScanReport previousReport = 
          reportDao.selectLatestReportForTarget(session, target.getTargetName());
      
      // Create a new report object for the current scan
      ScanReport currentReport = new ScanReport();
      currentReport.setTargetName(target.getTargetName());
      currentReport.setBucket(target.getBucket());
      currentReport.setFilePath(target.getPath());
      currentReport.setScanTimestamp(new Date(System.currentTimeMillis()));
      currentReport.setPreviousReport(previousReport);
      
      // Obtain previous file entries (if there were any)
      logger.info("Obtaining previous file entries (if any)");
      List<FileEntry> prevFileEntries = new LinkedList<>();
      if (previousReport != null) {
        prevFileEntries.addAll(fileDao.selectFilesForReport(session, previousReport));
      }
      Map<String, FileEntry> nameToPrevFileEntry = new HashMap<>();
      for (FileEntry dbEntry : prevFileEntries) {
        nameToPrevFileEntry.put(dbEntry.getFilename(), dbEntry);
      }
      logger.log(Level.INFO, "There were {0} file entries from a previous scan",
          prevFileEntries.size());
      
      // Match current file listing against previous scan
      Set<FileEntry> allFiles = new HashSet<>();
      Set<FileEntry> newFiles = new HashSet<>();
      Set<FileEntry> updatedFiles = new HashSet<>();
      Set<FileEntry> deletedFiles = new HashSet<>();
      Set<FileEntry> unchangedFiles = new HashSet<>();
      Set<String> currentScanFilenames = new HashSet<>();
      for (FileMetaData file : files) 
      {
        logger.info("Classifying file: "+file);
        currentScanFilenames.add(file.getName());
        System.out.println(file.getName());
        FileEntry prevFileEntry = nameToPrevFileEntry.get(file.getName());

        FileEntry newFileEntry = new FileEntry(
            target.getBucket(), target.getPath(), file.getName());
        newFileEntry.setModified(file.getLastModified());
        newFileEntry.setReport(currentReport);
        newFileEntry.setFileLength(file.getLength());
        allFiles.add(newFileEntry);
        
        if (prevFileEntry == null) {
          // This file wasn't in the previous scan
          logger.info("File: "+target.getBucket()+", "+target.getPath()+", "
              + file.getName()+" is new wrt last scan.");
          newFileEntry.setStatus(FileStatus.NEW);
          newFiles.add(newFileEntry);
        } else if (!datesEqual(prevFileEntry.getModified(), file.getLastModified())) {
          // Exists in the previous report but modification time has changed
          logger.info("File: "+target.getBucket()+", "+target.getPath()+", "
              + file.getName()+" is known, but the modification time has "
              + "changed: previous entry: "+prevFileEntry.getModified()
              + ", now: "+file.getLastModified());
          newFileEntry.setStatus(FileStatus.UPDATED);
          updatedFiles.add(newFileEntry);
        } else if (prevFileEntry.getFileLength() !=  file.getLength()) {
          // Exists in the previous report but file size has changed
          logger.info("File: "+target.getBucket()+", "+target.getPath()+", "
              + file.getName()+" is known, but the file size has changed.");
          newFileEntry.setStatus(FileStatus.UPDATED);
          updatedFiles.add(newFileEntry);          
        } else {
          logger.info("File: "+target.getBucket()+", "+target.getPath()+", "
              + file.getName()+" is known and has not changed.");
          newFileEntry.setStatus(FileStatus.UNCHANGED);
          unchangedFiles.add(newFileEntry);
        }
      }
      
      // Find deleted files
      Set<String> deletedFilenames = new HashSet<>(nameToPrevFileEntry.keySet());
      deletedFilenames.removeAll(currentScanFilenames);
      
      /*
       * Deleted files are those that did occur in the previous scan, but don't
       * now exist in the current scan. OR, they did occur in the previous scan
       * but were marked as 'DELETED'.
       */
      for (String deletedName : deletedFilenames) {
        FileEntry prevEntry = nameToPrevFileEntry.get(deletedName);
        if (prevEntry.getStatus().equals(FileStatus.DELETED)) {
          // This file 'existed' in the previous scan - but only as a DELETED event
          continue;
        }
        
        FileEntry newFileEntry = new FileEntry(
            target.getBucket(), target.getPath(), deletedName);
        newFileEntry.setReport(currentReport);
        newFileEntry.setStatus(FileStatus.DELETED);
        deletedFiles.add(newFileEntry);
        allFiles.add(newFileEntry);
      }
      
      //Set report summary
      currentReport.setTotalFileStatusChanges(files.size() + deletedFiles.size());
      
      currentReport.setTotalFiles(files.size());
      currentReport.setNewFiles(newFiles.size());
      currentReport.setUpdatedFiles(updatedFiles.size());
      currentReport.setUnchangedFiles(unchangedFiles.size());
      currentReport.setDeletedFiles(deletedFiles.size());
      
      // Write objects to database
      session.save(currentReport);
      session.flush(); //Ensure that the report is assigned a generated ID
//      logger.info("Stored scan report: "+currentReport);
      
      fileDao.store(session, newFiles);
      fileDao.store(session, updatedFiles);
      fileDao.store(session, unchangedFiles);
      fileDao.store(session, deletedFiles);

      //Create notifications for each event type, if they are required.
      Set<Message> msgs = new HashSet<>();
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, newFiles, target.getNewFileTopic()));
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, updatedFiles, target.getUpdatedFileTopic()));
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, unchangedFiles, target.getUnchangedFileTopic()));
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, deletedFiles, target.getDeletedFileTopic()));
      if (target.getDigestTopic() != null) {
        Set<FileEntry> allExceptUnchanged = new HashSet<>(allFiles);
        allExceptUnchanged.removeAll(unchangedFiles);
        // Publish summary message if there is at least one file change event
        if (!allExceptUnchanged.isEmpty()) {
          msgs.add(createDigestMessage(allExceptUnchanged, target.getDigestTopic()));
        }
      }
      
      runtime.getNotification().publish(msgs);
      session.getTransaction().commit();

      logger.info("Finished.");
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      logger.info("File scanning failed of: "
          +target.getBucket()+"; "+target.getPath());
      e.printStackTrace();
    }
    finally
    {
      SessionProvider.silentClose(session);
    }
  }
  
  
  private Set<Message> createIndividualFileNotifications(
      Map<String, FileEntry> nameToPrevFileEntry,
      Set<FileEntry> entries, String topicGuid)
      throws NotificationException, MbSerializerException
  {
    
    Set<Message> msgs = new HashSet<>();
    if (topicGuid == null) {
      return msgs;
    }
    
    for (FileEntry entry : entries) {
      //This is the main content of the message
      FileStateChanged stateChange = new FileStateChanged();
      stateChange.setFileMetaData(new FileMetaData(
          entry.getBucket(), entry.getFilePath(), entry.getFilename(), 
          entry.getFileLength(), entry.getModified()));
      stateChange.setStatusChange(entry.getStatus());
      stateChange.setScanReportId(entry.getReport().getId());

      String newStateChangeJson = JsonSerializer.serializeToString(stateChange);

      Map<String, String> content = new HashMap<>();
      content.put(Constants.MSG_KEY_NEW_FILE_STATE, newStateChangeJson);
      
      // Also include the previous state info (if present)
      if (nameToPrevFileEntry.containsKey(entry.getFilename())) {
        FileEntry prevEntry = nameToPrevFileEntry.get(entry.getFilename());
        FileStateChanged prevState = new FileStateChanged();
        prevState.setFileMetaData(new FileMetaData(
            prevEntry.getBucket(), prevEntry.getFilePath(), prevEntry.getFilename(),
            prevEntry.getFileLength(), prevEntry.getModified()));
        prevState.setStatusChange(prevEntry.getStatus());
        prevState.setScanReportId(prevEntry.getReport().getId());        
        
        String prevStateChangeJson = JsonSerializer.serializeToString(prevState);
        content.put(Constants.MSG_KEY_PREV_FILE_STATE, prevStateChangeJson);
      }

      Message msg = new Message();
      msg.setGuid(UidGenerator.generateUid());
      msg.setTopicGuid(topicGuid);
      msg.setPublisherGuid(PUBLISHER);
      msg.setContent(content);
      
      msgs.add(msg);
    }
    return msgs;
  }
  
  private Message createDigestMessage(Set<FileEntry> allFiles, String topicGuid) 
      throws MbSerializerException
  {
    Map<String, String> content = new HashMap<>();
    
    Message msg = new Message();
    msg.setGuid(UidGenerator.generateUid());
    msg.setTopicGuid(topicGuid);
    msg.setPublisherGuid(PUBLISHER);
    msg.setContent(content);
    
    
    for (FileEntry entry : allFiles) {
      //This is the main content of the message
      FileStateChanged stateChange = new FileStateChanged();
      stateChange.setFileMetaData(new FileMetaData(
          entry.getBucket(), entry.getFilePath(), entry.getFilename(), 
          entry.getFileLength(), entry.getModified()));
      stateChange.setStatusChange(entry.getStatus());
      stateChange.setScanReportId(entry.getReport().getId());

      String stateChangeJson = JsonSerializer.serializeToString(stateChange);

      content.put(entry.getFilename(), stateChangeJson);      
    }
    return msg;
  }
  
  
}
