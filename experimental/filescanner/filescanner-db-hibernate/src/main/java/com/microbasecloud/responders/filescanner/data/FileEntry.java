/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package com.microbasecloud.responders.filescanner.data;

import com.microbasecloud.filescanner.common.FileStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "files" )
public class FileEntry
    implements Serializable
{ 
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  @ManyToOne(cascade = CascadeType.ALL)
  private ScanReport report;
  
  @Column(nullable=false)
  private FileStatus status;
  
  @Column(nullable=false)
  private String bucket;
  @Column(nullable=false)
  private String filePath;
  @Column(nullable=false)
  private String filename;
  
  @Column(nullable=false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date modified;
  
  private long fileLength;

  public FileEntry()
  {
  }

  public FileEntry(String bucket, String path, String name)
  {
    this.bucket = bucket;
    this.filePath = path;
    this.filename = name;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder("FileInfo{");
    sb.append("bucket=").append(bucket)
        .append(", path=").append(filePath)
        .append(", name=").append(filename)
        .append(", status=").append(status)
        .append(", lastModified=").append(modified)
        .append(", fileLength=").append(fileLength)
        .append("}");
    return sb.toString();
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getBucket()
  {
    return bucket;
  }

  public void setBucket(String bucket)
  {
    this.bucket = bucket;
  }

  public String getFilePath()
  {
    return filePath;
  }

  public void setFilePath(String filePath)
  {
    this.filePath = filePath;
  }

  public String getFilename()
  {
    return filename;
  }

  public void setFilename(String filename)
  {
    this.filename = filename;
  }

  public ScanReport getReport()
  {
    return report;
  }

  public void setReport(ScanReport report)
  {
    this.report = report;
  }

  public Date getModified()
  {
    return modified;
  }

  public void setModified(Date modified)
  {
    this.modified = modified;
  }

  public long getFileLength()
  {
    return fileLength;
  }

  public void setFileLength(long fileLength)
  {
    this.fileLength = fileLength;
  }

  public FileStatus getStatus()
  {
    return status;
  }

  public void setStatus(FileStatus status)
  {
    this.status = status;
  }
}
