/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 16, 2012, 4:52:03 PM
 */

package com.microbasecloud.interproscan;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 * This class implements a simple Microbase responder. It is configured to
 * react to messages with the topic 'HelloTopic'. 
 * 
 * @author Keith Flanagan
 * @author Sirintra Nakjang
 */
public class InterproscanProcessor
    extends AbstractMessageProcessor
{
  private static final String TOPIC_IN = "IPRSCAN-JOB";
  private static final String TOPIC_OUT = "IPRSCAN-COMPLETED";
  
  /**
   * An array of two FileMetaData objects that represent the two input files
   * to be compared using BLAST.
   */
  private FileMetaData seqfile;
  
  /*
   * The remote filesystem location that will be used to upload the output files
   */
  private FileMetaData iprscanReportS3File;
  private String logFileS3Bucket;
  private String logFileS3Path;
   
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(300);
    info.setMaxLocalCores(50);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName("InterproscanProcessorResponder");
    info.setResponderVersion("INTERPROSCAN4.8-SNAPSHOT");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(TOPIC_IN);
    return info;
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    System.out.println("Doing preRun");
    seqfile = parseMessage(message);
    
    /*
     * Set the remote S3 location of where output file from this responder
     * will be uploaded to.
     */
    String output_remoteS3Bucket = seqfile.getBucket();
    String output_remoteS3Path = "output_iprscan/"
                              +seqfile.getName();
    String output_remoteS3Name = seqfile.getName()+".iprout";
    iprscanReportS3File = new FileMetaData(
        output_remoteS3Bucket, output_remoteS3Path, output_remoteS3Name);
    
    logFileS3Bucket = output_remoteS3Bucket;
    logFileS3Path = output_remoteS3Path;
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    System.out.println("Doing cleanup");
  }

  @Override
  public Set<Message> processMessage(Message incomingMessage)
      throws ProcessingException
  {
   System.out.println("Performing computational work");
    try
    {
      File inputFile = downloadInputFile(seqfile);
      
      File iprscanResult = runInterproscan(inputFile);
      uploadInterproscanReport(iprscanResult);
      
      InterproscanRawOutputParser.parse(iprscanResult);
      
      Message iprscanCompleteMsg = generateSuccessNotification(incomingMessage);
      
      Set<Message> messages = new HashSet<Message>();
      messages.add(iprscanCompleteMsg);
      // ... we could put additional messages here if necessary ...

      return messages;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run one or more commands: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
  }
  
   private FileMetaData parseMessage(Message m)
      throws ProcessingException
  {
    try
    {
      FileMetaData inputFile = new FileMetaData(
          m.getContent().get("input_bucket"),
          m.getContent().get("input_path"),
          m.getContent().get("input_filename"));
      return inputFile;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to parse message", e);
    }
  }
  
  private File downloadInputFile(FileMetaData remoteFile)
      throws ProcessingException
  {
    try
    {
      // Get a reference to the temporary directory for this job
      File tmpDir = getWorkingDirectory();
      
      //Download GBK file
      File destinationFile = new File(tmpDir, remoteFile.getName());
      
      getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
          remoteFile.getBucket(), remoteFile.getPath(), remoteFile.getName(), 
          destinationFile, true);
      
      return destinationFile;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to download remote file: " 
          + remoteFile.getBucket()+", "
          + remoteFile.getPath() + ", "
          + remoteFile.getName(), e);
    }
  }
  
  
  private File runInterproscan(File seqFile)
      throws ProcessingException
  {
    try
    {
      File iprscanResultFile = new File(getWorkingDirectory(), iprscanReportS3File.getName());
      String[] iprscanCommand = new String[]
      { "iprscan", "-cli",  //The iprscan executable file (must be on PATH)
            "-format", "raw",  // Specifies that InterproScan creates output in tabular format
            "-altjobs", //split a job into chunks and execute them one at a time
            "-iprlookup", //perform lookup for Interpro entry
            "-goterms", //perform Gene Ontology term lookup
            "-appl", "hmmpfam", //run HMMPfam
            "-appl", "superfamily",//run Superfamily
            "-appl", "patternscan", //run patternscan
            "-appl", "profilescan", //run profilescan
            "-appl", "seg",//run Seg
            "-appl", "coils",//run coils
            "-i", seqFile.getAbsolutePath(),   //FASTA-format query sequences
            "-o", iprscanResultFile.getAbsolutePath()}; //Output file
      executeCommand(iprscanCommand, "iprscan");
   
      return iprscanResultFile;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to execute InterproScan", e);
    }
  }

  
  private void executeCommand(String[] command, String stdOutPrefix)
      throws ProcessingException
  {
    File workDir = getWorkingDirectory();
    File stdOutFile = new File(workDir, stdOutPrefix+"-stdout.txt");
    File stdErrFile = new File(workDir, stdOutPrefix+"-stderr.txt");
    try {
      FileWriter stdOut = new FileWriter(stdOutFile);
      FileWriter stdErr = new FileWriter(stdErrFile);

      int exitStatus = NativeCommandExecutor.executeNativeCommand(
          workDir, command, stdOut, stdErr);

      stdOut.flush();
      stdOut.close();
      stdErr.flush();
      stdErr.close();

      if (exitStatus != 0) {
        throw new ProcessingException("Exit status of the command: "
            + Arrays.asList(command)
            + " \nwas "+exitStatus
            + ". Assuming that non-zero means that execution failed.");
      }
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to execute command: "+command[0], e);
    }
    finally {
      try {
        getRuntime().getMicrobaseFS().upload(
            stdOutFile, logFileS3Bucket, logFileS3Path, stdOutFile.getName(), null);
        getRuntime().getMicrobaseFS().upload(
            stdErrFile, logFileS3Bucket, logFileS3Path, stdErrFile.getName(), null);
      }
      catch(Exception e) {
        throw new ProcessingException(
            "Failed to upload STDOUT/STDERR content", e);
      }
    }
  }
  
  private void uploadInterproscanReport(File iprscanResultLocalFile)
      throws ProcessingException
  {
    try {
      getRuntime().getMicrobaseFS().upload(
          iprscanResultLocalFile, 
          iprscanReportS3File.getBucket(), iprscanReportS3File.getPath(), 
          iprscanReportS3File.getName(), null);
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to upload output file ", e);
    }
  }

  private Message generateSuccessNotification(Message parent)
  {
    Message successMsg = createMessage(
        parent,                 //The message to use as a parent
        TOPIC_OUT,              //The topic of the new message
        parent.getWorkflowStepId(),   //The workflow step ID
        //An optional human-readable description
        "Successfully completed a IPRSCAN for file: "
        + seqfile.getName());
    
    successMsg.getContent().put("input_bucket", seqfile.getBucket());
    successMsg.getContent().put("input_path", seqfile.getPath());
    successMsg.getContent().put("input_filename", seqfile.getName());

    successMsg.getContent().put("iprscan_result_bucket", iprscanReportS3File.getBucket());
    successMsg.getContent().put("iprscan_result_path", iprscanReportS3File.getPath());
    successMsg.getContent().put("iprscan_result_filename", iprscanReportS3File.getName());
    
    return successMsg;
  }
  
}
