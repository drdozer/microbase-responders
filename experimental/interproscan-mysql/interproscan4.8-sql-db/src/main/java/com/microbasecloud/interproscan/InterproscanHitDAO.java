/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.interproscan;

import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Sirintra Nakjang
 */
public class InterproscanHitDAO {
    private static final int DEFAULT_BATCH = 50000;
 
  public void store(Session session, Set<InterproscanHit> hits)
      throws InterproscanParserException
  {
    try
    {
      int count = 0;
      for (InterproscanHit hit : hits)
      {
        session.save(hit);
        count++;
        if (count % DEFAULT_BATCH == 0)
        {
          session.flush();
          session.clear();
        }
      }
    }
    catch(Exception e)
    {
      throw new InterproscanParserException(
          "Failed to execute store() operation", e);
    }
  }
 
  public List<InterproscanHit> selectHitsForReport(Session session, long reportId)
      throws InterproscanParserException
  {
    try
    {
      String qry = "from "+InterproscanHit.class.getName()+" where report.id = :id";
      Query query = session.createQuery(qry);
      query.setParameter("id", reportId);
 
      List<InterproscanHit> hits = query.list();
      return hits;
    }
    catch(Exception e)
    {
      throw new InterproscanParserException(
          "Failed to execute selectHitsForReport()", e);
    }
  }
}
