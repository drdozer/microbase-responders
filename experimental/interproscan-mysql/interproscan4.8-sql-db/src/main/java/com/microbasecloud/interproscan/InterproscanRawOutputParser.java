/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.interproscan;

import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import com.torrenttamer.util.UidGenerator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author Sirintra Nakjang
 */
public class InterproscanRawOutputParser {
    
  public InterproscanRawOutputParser()
  {
 
  }
 
  /**
   * Use this method for testing the parser from the command line
   */
  public static void main(String[] args)
      throws InterproscanParserException, HibernateUtilException
  {
    if (args.length != 1)
    {
      System.out.println("USAGE: <iprscan -cli -format raw -i inputfile>");
      System.exit(1);
    }
    String name = args[0];
 
    File file = new File(name);
    parse(file);
  }
  
  public static void parse(File file) throws InterproscanParserException, HibernateUtilException
  { 
 
    InterproscanRawOutputParser parser = new InterproscanRawOutputParser();
    Set<InterproscanHit> hits = parser.parseFromFile(file);
 
    System.out.println("Hits found: "+hits.size());
    System.out.println("Going to upload hits to specified database");
    SessionProvider interproscanSessionProvider =
      new SessionProvider("/InterproscanResponder-hibernate.cfg.xml");
 
    // Create a parent report object. Since we're not running this through
    // Microbase, just create a partial report object containing a unique ID
    // and a filename.
    InterproscanReport report = new InterproscanReport();
    report.setGuid(UidGenerator.generateUid()); //Unique ID for this Tmhmm report
    report.setSourceFileName(file.getName()); //Record name of result file
 
    //Associate all this parsed hits with this report
    for (InterproscanHit hit : hits)
    {
      hit.setReport(report);
    }
 
    /*
     * Store report and all associated HmmerHit objects to the database.
     */
    Session session = null;
    try
    {
      session = interproscanSessionProvider.getWriteableSession();
      session.beginTransaction();
      InterproscanHitDAO dao = new InterproscanHitDAO();
      dao.store(session, hits); //Stores the Interproscan Hit instances as well as the HmmerReport
      System.out.println("Committing...");
      session.getTransaction().commit();
      System.out.println("Committed.");
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      throw new InterproscanParserException("Failed to save "
        + hits.size()+" hits to the DB", e);
    }
    finally
    {
      SessionProvider.silentClose(session);
    }
 
    System.out.println("Done.");
  }
 
  public Set<InterproscanHit> parseFromFile(File interproscanReport)
      throws InterproscanParserException
  {
    try
    {
      FileInputStream fis = new FileInputStream(interproscanReport);
      return parse(fis);
    }
    catch(Exception e)
    {
      throw new InterproscanParserException(
          "Failed to parse Interproscan output from file: "+interproscanReport.getAbsolutePath(), e);
    }
  }
  public Set<InterproscanHit> parse(InputStream is) throws InterproscanParserException
  {
    try
    {
      Set<InterproscanHit> hits = new HashSet<InterproscanHit>();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      String line;
      while ((line = br.readLine()) != null)
      {
        if (line.startsWith("#"))
        {
          continue; //Skip comment lines
        }
        else if (line.trim().length() == 0)
        {
          continue; //Skip blank lines
        }
        InterproscanHit hit = parseHitLine(line);
        hits.add(hit);
      }
      return hits;
    }
    catch(Exception e)
    {
      throw new InterproscanParserException("Failed to parse content", e);
    }
  }
  
  private InterproscanHit parseHitLine(String line)
  {
    int numcol = 14;  
    String[] cols = line.split("\\t",14);
    if (cols.length == 11){
        numcol =11;
    }
    //System.out.println(cols.length);
    String qid = cols[0];
    String crc = cols[1];
    int length = Integer.parseInt(cols[2]);
    String method = cols[3];
    String hitid = cols[4];
    String hitDesc = cols[5];
    int startDom = Integer.parseInt(cols[6]);
    int endDom = Integer.parseInt(cols[7]);
    Double evalue = null;
    
    if (!cols[8].equals("NA"))
    {
        evalue = new Double(cols[8]);
    }
    
    String status = cols[9];
    String iprAcc = null;
    if (numcol > 11){
        iprAcc = cols[11];
    }
    
    String iprDesc = null;
    if (numcol > 12){
        iprDesc = cols[12];
    }
    String goTerm = null;
    
    if ((cols[3].equals("Seg")) || (cols[3].equals("Coil")) || !(numcol > 13))
    {
        goTerm = null;    
    }
    else
    {
        goTerm = cols[13];
    }
      
    InterproscanHit hit = new InterproscanHit();
    hit.setQueryAcc(qid);
    hit.setCrc(crc);
    hit.setSeqlength(length);
    hit.setMethod(method);
    hit.setHitAcc(hitid);
    hit.setHitDescription(hitDesc);
    hit.setStartDomain(startDom);
    hit.setEndDomain(endDom);
    hit.setEvalue(evalue);
    hit.setStatus(status);
    hit.setIprAcc(iprAcc);
    hit.setIprDescription(iprDesc);
    hit.setGoDescription(goTerm);
    
    return hit;
  }
    
}
