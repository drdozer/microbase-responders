/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.prodigal.responder;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class BlastResponder
        extends AbstractMessageProcessor {

    private static final String TOPIC_IN = "PAIRWISE-BLAST-JOB";
    private static final String TOPIC_OUT = "PAIRWISE-BLAST-COMPLETED";
    /**
     * Array of FileMetaData[] objects created to store the two input files for
     * comparison
     *
     * @return
     */
    private FileMetaData[] pair;
    /**
     * the remote filesystem location that will be used to upload the output
     * files to
     *
     * @return
     */
    private FileMetaData blastReportS3File;
    private String logFileS3Bucket;
    private String logFileS3Path;

    @Override
    protected ResponderInfo createDefaultResponderInfo() {
        ResponderInfo info = new ResponderInfo();
        info.setEnabled(true);
        info.setMaxDistributedCores(300);
        info.setMaxLocalCores(50);
        info.setPreferredCpus(1);
        info.setQueuePopulatorLastRunTimestamp(0);
        info.setQueuePopulatorRunEveryMs(30000);
        info.setResponderName(BlastResponder.class.getSimpleName());
        info.setResponderVersion("1.0");
        info.setResponderId(info.getResponderName() + "-" + info.getResponderVersion());
        info.setTopicOfInterest(TOPIC_IN);
        return info;

    }

    @Override
    public void preRun(Message msg) throws ProcessingException {
        System.out.println("Doing preRun");
        pair = parseMessage(msg); // parseMessage extracts information about the two files like bucket name, path and name
        /*
         * Set the remote S3 location of where output file from this responder
         * will be uploaded to.
         */
        String remoteS3Bucket = pair[0].getBucket();
        String remoteS3Path = "pairwise-blasts/"
                + pair[0].getName() + "-vs-" + pair[1].getName();
        String remoteS3Name = pair[0].getName() + "-" + pair[1].getName() + ".blast";
        blastReportS3File = new FileMetaData(
                remoteS3Bucket, remoteS3Path, remoteS3Name);
        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;

    }

    @Override
    public void cleanupPreviousResults(Message msg) throws ProcessingException {
    }

    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        try {
            File subjectFile = downloadInputFile(pair[0]);
            File queryFile = downloadInputFile(pair[1]);

            File blastResult = runBlast(subjectFile, queryFile);
            uploadBlastReport(blastResult);

            Message blastCompleteMsg = generateSuccessNotification(msg);

            Set<Message> messages = new HashSet<Message>();
            messages.add(blastCompleteMsg);

            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: " + 
                    "on: " + getRuntime().getNodeInfo().getHostname() + "in: " +
                    getWorkingDirectory(), e);

        }
    }

    private FileMetaData[] parseMessage(Message m) throws ProcessingException {

        try {
            FileMetaData subjectFile = new FileMetaData(m.getContent().get("subject_bucket"), 
                    m.getContent().get("subject_path"),
                    m.getContent().get("subject_filename"));
            FileMetaData queryFile = new FileMetaData(m.getContent().get("target_bucket"), 
                    m.getContent().get("target_path"),
                    m.getContent().get("target_filename"));

            return new FileMetaData[]{subjectFile, queryFile};

        } catch (Exception e) {
            throw new ProcessingException("Failed to generate parse message", e);
        }

    }

    private File downloadInputFile(FileMetaData remoteFile)
            throws ProcessingException {
        try {
            //get a reference t othe working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the gbk file
            File destinationFile = new File(tmpDir, remoteFile.getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
                    remoteFile.getBucket(), remoteFile.getPath(), 
                    remoteFile.getName(), destinationFile, true);

            return destinationFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file" + 
                    remoteFile.getBucket() + " , " + remoteFile.getPath());
        }
    }

    private File runBlast(File subjectFile, File queryFile) throws 
            ProcessingException {
        try {
            String[] fastaCommand = new String[]{"formatdb", "-p", "F", "-i", 
                subjectFile.getAbsolutePath()};
            executeCommand(fastaCommand, "formatdb");

            File blastResultFile = new File(getWorkingDirectory(), 
                    blastReportS3File.getName());

            String[] blastCommand = new String[]{"blastall", "-p", "blastn",
                "-m", "8", "-i", queryFile.getAbsolutePath(), "-d", 
                subjectFile.getAbsolutePath(),
                "-o", blastResultFile.getAbsolutePath()};
            executeCommand(blastCommand, "blast");

            return blastResultFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute blast", e);
        }
    }

    private void uploadBlastReport(File blastResultLocalFile)
            throws ProcessingException {
        try {
            getRuntime().getMicrobaseFS().upload(
                    blastResultLocalFile,
                    blastReportS3File.getBucket(), 
                    blastReportS3File.getPath(),
                    blastReportS3File.getName(), null);
        } catch (Exception e) {
            throw new ProcessingException("Failed to upload output file ", e);
        }

    }

    private Message generateSuccessNotification(Message parent) {
        {
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully completed a pairwise BLAST-N between sequences: "
                    + pair[0].getName() + " and " + pair[1].getName());
            successMsg.getContent().put("subject_bucket", pair[0].getBucket());
            successMsg.getContent().put("subject_path", pair[0].getPath());
            successMsg.getContent().put("subject_filename", pair[0].getName());
            successMsg.getContent().put("target_bucket", pair[1].getBucket());
            successMsg.getContent().put("target_path", pair[1].getPath());
            successMsg.getContent().put("target_filename", pair[1].getName());
            successMsg.getContent().put("blast_result_bucket",
                    blastReportS3File.getBucket());
            successMsg.getContent().put("blast_result_path", 
                    blastReportS3File.getPath());
            successMsg.getContent().put("blast_result_filename",
                    blastReportS3File.getName());
            return successMsg;
        }

    }

    private void executeCommand(String[] command, String stdOutPrefix) 
            throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir,
                    command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: " +
                        Arrays.asList(command) + "\nwas " + exitStatus + 
                        ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: " +
                    command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, logFileS3Bucket, 
                        logFileS3Path, stdOutFile.getName(), null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, logFileS3Bucket, 
                        logFileS3Path, stdErrFile.getName(), null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }
}
