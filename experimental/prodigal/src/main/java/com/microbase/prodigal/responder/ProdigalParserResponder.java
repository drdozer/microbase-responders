/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.prodigal.responder;

import com.microbasecloud.prodigal.datamodel.ProdigalReport;
import com.microbasecloud.prodigal.parser.ProdigalDatabaseParser;
import com.microbasecloud.prodigal.parser.ProdigalProteinDatabaseParser;
import com.torrenttamer.util.UidGenerator;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;

/**
 *
 * @author prisni
 */
public class ProdigalParserResponder extends AbstractMessageProcessor {

    private static final String TOPIC_IN = "GENE-PREDICTION-ACCOMPLISHED";
    private static final String TOPIC_OUT = "GENE-PREDICTION-PARSED";
   
    
//    private String logFileS3Bucket;
//    private String logFileS3Path;

    @Override
    protected ResponderInfo createDefaultResponderInfo() {
        ResponderInfo info = new ResponderInfo();
        info.setEnabled(true);
        info.setMaxDistributedCores(300);
        info.setMaxLocalCores(50);
        info.setPreferredCpus(1);
        info.setQueuePopulatorLastRunTimestamp(0);
        info.setQueuePopulatorRunEveryMs(30000);
        info.setResponderName(ProdigalParserResponder.class.getSimpleName());
        info.setResponderVersion("1.0");
        info.setResponderId(info.getResponderName() + "-"
                + info.getResponderVersion());
        info.setTopicOfInterest(TOPIC_IN);
        return info;
    }

    @Override
    public void preRun(Message msg) throws ProcessingException {
        System.out.println("Doing pre-run");
    }

    @Override
    public void cleanupPreviousResults(Message msg) throws ProcessingException {
    }

    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        try {
            /*
             * Parse message content
             */
            System.out.println("Parsing incoming message");

            
            FileMetaData genePredFileMetaData = new FileMetaData(
                    msg.getContent().get("result_file.predicted_genes.bucket"),
                    msg.getContent().get("result_file.predicted_genes.path"),
                    msg.getContent().get("result_file.predicted_genes.filename"));

            FileMetaData translationPredFileMetaData = new FileMetaData(
                    msg.getContent().get("result_file.predicted_translations.bucket"),
                    msg.getContent().get("result_file.predicted_translations.path"),
                    msg.getContent().get("result_file.predicted_translations.filename"));
            
            
            /*
             * Download necessary input files
             */
            System.out.println("Downloading required files");
            File genePredFile = downloadInputFile(genePredFileMetaData);
            File translationPredFile = downloadInputFile(translationPredFileMetaData);
            
            
            /*
             * Execute required analysis tool(s)
             */
            ProdigalReport report = new ProdigalReport();
            report.setGuid(UidGenerator.generateUid());
            report.setIncomingMessageId(msg.getGuid());
            report.setCdsListFileBucket(genePredFileMetaData.getBucket());
            report.setCdsListSourceFilePath(genePredFileMetaData.getPath());
            report.setCdsListSourceFilePath(genePredFileMetaData.getName());
            
            ProdigalDatabaseParser.parse(genePredFile, report);
            
            //Parse translations
            report.setTranslationListFileBucket(translationPredFileMetaData.getBucket());
            report.setTranslationListSourceFilePath(translationPredFileMetaData.getPath());
            report.setTranslationListSourceFileName(translationPredFileMetaData.getName());
            ProdigalProteinDatabaseParser.parse(translationPredFile, report);
            
            
            
            /*
             * Upload necessary output files
             */
            
            
            /*
             * Generated 'success' notification with enough information for
             * the next responder.
             */
            
            Message prodigalPredictMsg = generateSuccessNotification(msg);
            Set<Message> messages = new HashSet<Message>();
            messages.add(prodigalPredictMsg);

            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);

        }
    }

    private File downloadInputFile(FileMetaData remoteFile) throws
            ProcessingException {
        try {
            //get a reference to the working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the fasta file
            File destinationFile = new File(tmpDir, remoteFile.getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
                    remoteFile.getBucket(), remoteFile.getPath(),
                    remoteFile.getName(), destinationFile, true);

            return destinationFile;

        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file"
                    + remoteFile.getBucket() + " , " + remoteFile.getPath());
        }
    }

   



    private Message generateSuccessNotification(Message parent) {
        {
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully accomplished parsing genes to the DB: ");
//            successMsg.getContent().put("seq_bucket", seq.getBucket());
//            successMsg.getContent().put("seq_path", seq.getPath());
//            successMsg.getContent().put("seq_filename", seq.getName());
//            successMsg.getContent().put("predict_result_bucket", genePredictionS3File.getBucket());
//            successMsg.getContent().put("predict_result_path", genePredictionS3File.getPath());
//            successMsg.getContent().put("predict_result_filename", genePredictionS3File.getName());
            return successMsg;
        }

    }

   
    
}
