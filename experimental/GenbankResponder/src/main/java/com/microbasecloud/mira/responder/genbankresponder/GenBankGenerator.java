/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.mira.responder.genbankresponder;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class GenBankGenerator extends AbstractMessageProcessor{
    
    private static final String TOPIC_IN = "GENBANK-FILE-CREATION-JOB";
    private static final String TOPIC_OUT = "GENBANK-REPORT-CREATED";
    
    private FileMetaData templateFile;
    private FileMetaData[] inputFiles;
    private FileMetaData[] outputS3File;
    private String logFileS3Bucket;
    private String logFileS3Path;

    public GenBankGenerator() {
        outputS3File = new FileMetaData[3];
    }
    
    

    @Override
    protected ResponderInfo createDefaultResponderInfo() {
         ResponderInfo info = new ResponderInfo();
        info.setEnabled(true);
        info.setMaxDistributedCores(300);
        info.setMaxLocalCores(50);
        info.setPreferredCpus(1);
        info.setQueuePopulatorLastRunTimestamp(0);
        info.setQueuePopulatorRunEveryMs(30000);
        info.setResponderName(GenBankGenerator.class.getSimpleName());
        info.setResponderVersion("1.0");
        info.setResponderId(info.getResponderName() + "-"
                + info.getResponderVersion());
        info.setTopicOfInterest(TOPIC_IN);
        return info;
    }

    public void preRun(Message msg) throws ProcessingException {
         System.out.println("Doing pre-run");
        templateFile = parseTemplateMessage(msg);
        inputFiles = parseInputMessage(msg);
                // set remote location for output
        String fileName = inputFiles[0].getName();
        int i = 0;
        int j = fileName.length()-4;
        String actualFileName = fileName.substring(i,j-1);
        String remoteS3Bucket = inputFiles[0].getBucket();
        String remoteS3Path = "genbank file for: " + inputFiles[0].getName();
        String remoteGenbankS3Name = actualFileName + ".gbf";
        System.out.println(remoteGenbankS3Name);
        outputS3File[0] = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteGenbankS3Name);
        String remoteValS3Name =  actualFileName + ".val";
        outputS3File[1] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteValS3Name);
        String remoteSqnS3Name =  actualFileName + ".sqn";
        outputS3File[2] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteSqnS3Name);
        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;
    }

    public void cleanupPreviousResults(Message msg) throws ProcessingException {
        
    }

    public Set<Message> processMessage(Message msg) throws ProcessingException {
        try {
            File template = downloadInputFile(templateFile);
            File fastaFile = downloadInputFile(inputFiles[0]);
            File featureTable = downloadInputFile(inputFiles[1]);
            File[] outputsLocalFile = runTbl2Asn(template);
                    
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[0],
                    outputS3File[0].getBucket(),
                    outputS3File[0].getPath(),
                    outputS3File[0].getName(), null);
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[1],
                    outputS3File[1].getBucket(),
                    outputS3File[1].getPath(),
                    outputS3File[1].getName(), null);
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[2],
                    outputS3File[2].getBucket(),
                    outputS3File[2].getPath(),
                    outputS3File[2].getName(), null);
            Message genbankFileCreatedMsg = generateSuccessNotification(msg);

            Set<Message> messages = new HashSet<Message>();
            messages.add(genbankFileCreatedMsg);
            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);
        }
    }
   

    private File downloadInputFile(FileMetaData remoteFile)
            throws ProcessingException {
        try {
            //get a reference to the working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the gbk file
            File destinationFile = new File(tmpDir, remoteFile.getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
                    remoteFile.getBucket(), remoteFile.getPath(), 
                    remoteFile.getName(), destinationFile, true);

            return destinationFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file" + 
                    remoteFile.getBucket() + " , " + remoteFile.getPath());
        }
    }

    private FileMetaData[] parseInputMessage(Message m) 
            throws ProcessingException {
        try {

            FileMetaData fastaFile = new FileMetaData(m.getContent().
                    get("fasta_bucket"),
                    m.getContent().get("fasta_path"),
                    m.getContent().get("fasta_filename"));
            FileMetaData featureFile = new FileMetaData(m.getContent().
                    get("feature_bucket"),
                    m.getContent().get("feature_path"),
                    m.getContent().get("feature_filename"));

            return new FileMetaData[]{fastaFile, featureFile};
        } catch (Exception e) {
            throw new ProcessingException("Failed to generate parse message", e);
        }

    }


    private FileMetaData parseTemplateMessage(Message m) 
            throws ProcessingException {
        try{
            
              FileMetaData template = new FileMetaData(m.getContent().
                      get("template_bucket"),
                    m.getContent().get("template_path"),
                    m.getContent().get("template_filename"));
              return template;
        } catch (Exception e) {
            throw new ProcessingException("Failed to generate parse message");
        }
        
    }

    private File[] runTbl2Asn(File template)
        throws ProcessingException {
        try {
//            File currDirFile = getWorkingDirectory();
             String[] genbankCommand = new String[]{
               "/mnt/responder_data/linux64.tbl2asn",
               "-t", template.getAbsolutePath(),
               "-p", getWorkingDirectory().getAbsolutePath(), 
               "-V", "vb"};
            executeCommand(genbankCommand, "genbankgenerator");

            File genbankResultFile = new File(getWorkingDirectory(), 
                    outputS3File[0].getName());
            File validateResultFile = new File(getWorkingDirectory(),
                    outputS3File[1].getName());
            File sequinResultFile = new File(getWorkingDirectory(), 
                    outputS3File[2].getName());

             return new File[]{genbankResultFile, validateResultFile, 
                 sequinResultFile};
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute Tbl2Asn", e);
        }
    }

    private void executeCommand(String[] command, String stdOutPrefix) 
            throws ProcessingException {
        System.out.println("*********************: Executing command: "+
                Arrays.asList(command));
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir,
                    command, stdOut, stdErr);
            System.out.println("Exit status of command: "+exitStatus);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: " +
                        Arrays.asList(command) + "\nwas " + exitStatus + 
                        ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: " +
                    command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, logFileS3Bucket, 
                        logFileS3Path, stdOutFile.getName(), null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, logFileS3Bucket, 
                        logFileS3Path, stdErrFile.getName(), null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }

       private Message generateSuccessNotification(Message parent) {
        {
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully generated a genbank file for the provided fasta file "
                    + inputFiles[0].getName() + " and features table file " + 
                    inputFiles[1].getName());
            successMsg.getContent().put("fasta_bucket", inputFiles[0].getBucket());
            successMsg.getContent().put("fasta_path", inputFiles[0].getPath());
            successMsg.getContent().put("fasta_filename", inputFiles[0].getName());
            successMsg.getContent().put("features_bucket", inputFiles[1].getBucket());
            successMsg.getContent().put("features_path", inputFiles[1].getPath());
            successMsg.getContent().put("features_filename", inputFiles[1].getName());
            successMsg.getContent().put("genbank_result_bucket",
                    outputS3File[0].getBucket());
            successMsg.getContent().put("genbank_result_path", 
                    outputS3File[0].getPath());
            successMsg.getContent().put("genbank_result_filename",
                    outputS3File[0].getName());
            successMsg.getContent().put("validate_result_bucket",
                    outputS3File[1].getBucket());
            successMsg.getContent().put("validate_result_path", 
                    outputS3File[1].getPath());
            successMsg.getContent().put("validate_result_filename",
                    outputS3File[1].getName());
            successMsg.getContent().put("sqn_result_bucket",
                    outputS3File[2].getBucket());
            successMsg.getContent().put("sqn_result_path", 
                    outputS3File[2].getPath());
            successMsg.getContent().put("sqn_result_filename",
                    outputS3File[2].getName());
            return successMsg;
        }

    }
    
}
