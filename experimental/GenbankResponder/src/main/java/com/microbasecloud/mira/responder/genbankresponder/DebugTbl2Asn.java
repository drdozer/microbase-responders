/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 23-Oct-2012, 10:34:51
 */

package com.microbasecloud.mira.responder.genbankresponder;

import java.io.File;
import java.util.Arrays;
import uk.org.microbase.util.cli.NativeCommandException;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author Keith Flanagan
 */
public class DebugTbl2Asn
{
  public static void main(String[] args) 
      throws NativeCommandException
  {
    StringBuilder stdOut = new StringBuilder();
    StringBuilder stdErr = new StringBuilder();
    
    File template = new File("/tmp/test/template.sbt");
    File workingDir = new File("/tmp/test");
    
    //original - fails due to concattenated '-p ' + <path>
//    String[] command = new String[]{
//      "/mnt/responder_data/linux64.tbl2asn",
//        "-t", template.getAbsolutePath(),
//        "-p "+workingDir.getAbsolutePath(), 
//      "-V vb"};
    
    //Fixed - succeeds because every value is it's own String
    String[] command = new String[]{
      "/tmp/test2/linux64.tbl2asn",
        "-t", template.getAbsolutePath(),
        "-p", workingDir.getAbsolutePath(), 
        "-V", "vb"};
    
    System.out.println("Executing command line: \n"+Arrays.asList(command));
    
    int exitStatus = NativeCommandExecutor.executeNativeCommand(
        workingDir, command, stdOut, stdErr);
    
    
    System.out.println("Execution summary:\n"
        + "  * Exit status: "+exitStatus
        + "\nSTDOUT:\n" + stdOut
        +" \n\nSTDERR:\n" + stdErr);
  }
}
