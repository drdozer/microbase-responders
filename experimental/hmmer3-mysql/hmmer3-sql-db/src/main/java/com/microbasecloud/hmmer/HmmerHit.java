/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */

package com.microbasecloud.hmmer;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "hmmer_hits" )
public class HmmerHit 
    implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  @ManyToOne(cascade = CascadeType.ALL)
  private HmmerReport report;
  
  private String targetName;
  private String targetAcc;
  private String queryName;
  private String queryAcc;
  private double fullSeqEVal;
  private double fullSeqScore;
  private double fullSeqBias;
    
  private double best1DomSeqEVal;
  private double best1DomSeqScore;
  private double best1DomSeqBias;
    
  private double domNumExp;
  private int domNumReg;
  private int domNumClu;
  private int domNumOv;
  private int domNumEnv;
  private int domNumDom;
  private int domNumRep;
  private int domNumInc;
    
  private String descr;
  
      
      
  public HmmerHit()
  {
  }
  

  @Override
  public String toString()
  {
    return "HmmerHit{" + "id=" + id + ", targetName=" + 
        targetName + ", targetAcc=" + targetAcc + ", queryName=" + 
        queryName + ", queryAcc=" + queryAcc + ", fullSeqEVal=" +
        fullSeqEVal + ", fullSeqScore=" + fullSeqScore + ", fullSeqBias=" + 
        fullSeqBias + ", best1DomSeqEVal=" + best1DomSeqEVal + ", best1DomSeqScore=" + 
        best1DomSeqScore + ", best1DomSeqBias=" + best1DomSeqBias + ", domNumExp=" + 
        domNumExp + ", domNumReg=" + domNumReg + ", domNumClu=" + 
        domNumClu + ", domNumOv=" + domNumOv + ", domNumEnv=" + 
        domNumEnv + ", domNumDom=" + domNumDom + ", domNumRep=" + 
        domNumRep + ", domNumInc=" + domNumInc + ", descr=" + descr + '}';
  }
  

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public double getBest1DomSeqBias()
  {
    return best1DomSeqBias;
  }

  public void setBest1DomSeqBias(double best1DomSeqBias)
  {
    this.best1DomSeqBias = best1DomSeqBias;
  }

  public double getBest1DomSeqEVal()
  {
    return best1DomSeqEVal;
  }

  public void setBest1DomSeqEVal(double best1DomSeqEVal)
  {
    this.best1DomSeqEVal = best1DomSeqEVal;
  }

  public double getBest1DomSeqScore()
  {
    return best1DomSeqScore;
  }

  public void setBest1DomSeqScore(double best1DomSeqScore)
  {
    this.best1DomSeqScore = best1DomSeqScore;
  }

  public String getDescr()
  {
    return descr;
  }

  public void setDescr(String descr)
  {
    this.descr = descr;
  }

  public int getDomNumClu()
  {
    return domNumClu;
  }

  public void setDomNumClu(int domNumClu)
  {
    this.domNumClu = domNumClu;
  }

  public int getDomNumDom()
  {
    return domNumDom;
  }

  public void setDomNumDom(int domNumDom)
  {
    this.domNumDom = domNumDom;
  }

  public int getDomNumEnv()
  {
    return domNumEnv;
  }

  public void setDomNumEnv(int domNumEnv)
  {
    this.domNumEnv = domNumEnv;
  }

  public double getDomNumExp()
  {
    return domNumExp;
  }

  public void setDomNumExp(double domNumExp)
  {
    this.domNumExp = domNumExp;
  }

  public int getDomNumInc()
  {
    return domNumInc;
  }

  public void setDomNumInc(int domNumInc)
  {
    this.domNumInc = domNumInc;
  }

  public int getDomNumOv()
  {
    return domNumOv;
  }

  public void setDomNumOv(int domNumOv)
  {
    this.domNumOv = domNumOv;
  }

  public int getDomNumReg()
  {
    return domNumReg;
  }

  public void setDomNumReg(int domNumReg)
  {
    this.domNumReg = domNumReg;
  }

  public int getDomNumRep()
  {
    return domNumRep;
  }

  public void setDomNumRep(int domNumRep)
  {
    this.domNumRep = domNumRep;
  }

  public double getFullSeqBias()
  {
    return fullSeqBias;
  }

  public void setFullSeqBias(double fullSeqBias)
  {
    this.fullSeqBias = fullSeqBias;
  }

  public double getFullSeqEVal()
  {
    return fullSeqEVal;
  }

  public void setFullSeqEVal(double fullSeqEVal)
  {
    this.fullSeqEVal = fullSeqEVal;
  }

  public double getFullSeqScore()
  {
    return fullSeqScore;
  }

  public void setFullSeqScore(double fullSeqScore)
  {
    this.fullSeqScore = fullSeqScore;
  }

  public String getQueryAcc()
  {
    return queryAcc;
  }

  public void setQueryAcc(String queryAcc)
  {
    this.queryAcc = queryAcc;
  }

  public String getQueryName()
  {
    return queryName;
  }

  public void setQueryName(String queryName)
  {
    this.queryName = queryName;
  }

  public String getTargetAcc()
  {
    return targetAcc;
  }

  public void setTargetAcc(String targetAcc)
  {
    this.targetAcc = targetAcc;
  }

  public String getTargetName()
  {
    return targetName;
  }

  public void setTargetName(String targetName)
  {
    this.targetName = targetName;
  }

  public HmmerReport getReport()
  {
    return report;
  }

  public void setReport(HmmerReport report)
  {
    this.report = report;
  }
}
